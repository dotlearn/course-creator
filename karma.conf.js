// Karma configuration
// Generated on Sun Nov 09 2014 12:56:37 GMT-0500 (EST)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
      'build/base/js/lib/angular/angular.min.js',
      'build/base/js/lib/*.js',
      'build/base/js/lib/il8n.js',
      'build/base/js/lib/jquery/jquery.js',
      'build/base/js/lib/jquery/jquery-ui.js',
      'build/base/js/lib/zip/zip.js',
      'build/base/js/lib/zip/zip-ext.js',
      {pattern: 'build/base/js/lib/zip/inflate.js', watched: true, included: false, served: true},
      {pattern: 'build/base/js/lib/zip/deflate.js', watched: true, included: false, served: true},
      {pattern: 'build/base/locales/**', watched: true, included: false, served: true},
      'build/base/js/lib/bootstrap/ui-bootstrap.js',
/*      'tests/jasmine/lib/jasmine-2.0.0/jasmine.js',
      'tests/jasmine/lib/jasmine-2.0.0/jasmine-html.js',
        'tests/jasmine/lib/jasmine-2.0.0/boot.js',
      'tests/jasmine/lib/jasmine-2.0.0/jasmine.css',*/
      'tests/jasmine/lib/angular-mocks.js',
      'tests/jasmine/spec/*.js',
      'tests/jasmine/spec/common/*.js',
      'tests/jasmine/mock-lsns/*.js',
      {pattern: 'build/base/modules/**', watched: true, included: false, served: true},
      'build/base/js/*.js'
    ],

    // list of files to exclude
     exclude: [ 'build/base/js/main.js'],

    proxies :  {
      '/js': 'http://localhost:9876/base/build/base/js',
      '/il8n': 'http://localhost:9876/base/build/base/il8n',
      '/locales': 'http://localhost:9876/base/build/base/locales',
      '/modules': 'http://localhost:9876/base/build/base/modules'
    },

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Firefox'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true
  });
};
