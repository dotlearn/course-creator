/**
 * Created by sam on 1/23/15.
 */

angular.module('auth', ['angular-storage']).
    factory('authenticator', function(store){

        return {

            credentials: {},

            setAuthCredentials: function (credentials) {


                store.set('credentials', credentials);

            },

            clearCredentials: function () {

                store.remove('credentials');
            },


            getAuthCredentials: function () {

                return store.get('credentials');

            }

        };

    });