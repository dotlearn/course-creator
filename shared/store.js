/*====================================================================================================================================

Project: dot Learn

Application: HTML Web app

Version: v0.6.0

Author: Sam Bhattacharyya

File: status.js

Description: AngularJS module for interfacing with IndexedDB, retrieving lesson and course objects from the object store.

=======================================================================================================================================*/

angular.module('store', []).
value('db', {}).
factory('dBstore', function (db) {

	return {
  			
		get: function (type, id, callback) {
		     
		     	var objectStore = this.getStore(type);
				var request = objectStore.get(id);
				
				 request.onsuccess = function(event){
				 	  callback(request.result);
				 	  // $log.log('Successfully Retrieved ' + type + ' ' + id + ' from IndexedDB');
				 	  util.debug.log('Successfully Retrieved ' + type + ' ' + id + ' from IndexedDB');
				 };
				 
				 request.onerror = function (event) {
                 util.debug.warn('Error loading ' + type + ' '+ id + ' from IndexedDB' + event.target.errorCode);  				 
				 };
		 
		},
		
		remove: function (type, id, callback, onerror) {
		
				var objectStore = this.getStore(type);	
 	         var request = objectStore.delete(id);
	       		request.onsuccess = function(event){
	       			
	       		    callback();		       		    
	       		    util.debug.log('Successfully Removed ' + type + ' ' + id + ' from IndexedDB');
 			      };
 			      
 					request.onerror = function(event){
 						
 						 util.debug.warn('Error loading  ' + type + ' ' + id + 'from IndexedDB' + event.target.errorCode);
 						
 						onerror(event);

		    };
	     },
		
		set: function (type, object, callback, onerror) {
			
			
						onerror = onerror || function () {};
						callback = callback || function () {};
		         
		          var objectStore = this.getStore(type);
		           
		          var request  = objectStore.put(object);
		          
					request.onsuccess = function(event){
						
						   callback();
				         util.debug.log('Successfully saved ' + type + ' ' + object.id + ' to IndexedDB');

		          };
		          
		          request.onerror = function (event) {
		          	
		          		util.debug.warn('Error adding  ' + type + ' ' + id + 'to  IndexedDB : ' + event.target.errorCode);

		                   
                  onerror();		          
		          };
		
		},
	
	   list: function (type, callback) {
	   	
	            	
           var objectStore = this.getStore(type);	
            var files = [];	
            
            util.debug.log('Listing all files of type '+ type+ ' in the store', false );
   
           objectStore.openCursor().onsuccess = function(event) {     // Open up Cursor to get all courses 	   	
	   	                        
				    	  
				        var cursor = event.target.result;  
	
				        if (cursor) {
						    files.push(cursor.value);
			             cursor.continue();  
				          }   else {
				          	 util.debug.log('Finished Listing', false );
			           	    callback(files);
			            }
			        
           };
     },
 
 
     getStore: function (type) {
     	
     	       

				  var transaction = db.transaction([type], "readwrite");
			
					transaction.oncomplete = function(event) {  
					};
					
					transaction.onerror = function(event) {
						
						 util.debug.warn('Failed to fetch IndexDB store of ' + type + ': ' + event.target.errorCode);  
                            
					};		
					
					return transaction.objectStore(type);
     },
     
     deleteAll: function (type, callback, error) {
     	
                  onerror = onerror || function () {};
						callback = callback || function () {};
     	
                  var objectstore = this.getStore(type);

     					var req = objectstore.clear();
								req.onsuccess = function () {
								    util.debug.info("Deleted " + type + "successfully");
								    callback();
								};
								req.onerror = function () {
								    util.debug.info("Couldn't delete " + type);
								    onerror();
								};
								req.onblocked = function () {
								    util.debug.info("Couldn't delete database due to the operation being blocked");
								    onerror();
								};
     
     },
     
     init: function (callback) {

		 callback = callback || function(){};
			     window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
			     window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;


               var handler = this;


					var request = indexedDB.open("Test 6");            // Open database (test)
					

					
					request.onerror = function(event) {                 // Error Creating database 
	 				  
					  util.debug.warn("Database Error: " + event.target.errorCode);
					  
					};
					request.onsuccess = function(event) {              
					  db = request.result;                          // Store resulting database in db
					  util.debug.info("Indexed DB Database loaded");
						callback();
			
					};
					
					request.onupgradeneeded = function(event) {      // Database doesn't exist yet (create it)
					   db = event.target.result;
					
				      var courseStore = db.createObjectStore("course", { keyPath: "id" });
				      
				      
                  var elessonStore = db.createObjectStore("e-lesson", { keyPath: "id" });
					  
                  var lessonStore = db.createObjectStore("lesson", { keyPath: "id" });
                  
					  
					   util.debug.info("Instatiated new database");
					};

     
     } 
	   
	
	
                 
		
	};


}).
run(function (dBstore) {
dBstore.init();
});

