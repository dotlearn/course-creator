/**
 * Created by sam on 1/25/15.
 */
angular.module('login', ['server', 'auth'])
    .value('loggedIn', false)
    .value('queue', [])
.factory('login', function(authenticator, server, loggedIn, queue){

var login = {

                    loggedIn: function(){

                        return loggedIn;
                    } ,


                    credentials: {

                    },


                    onLogin: function (onLoginFunction) {

                        if(loggedIn)
                            return onLoginFunction();
                        else
                          queue.push(onLoginFunction);
                    },

                    onLoginExecute: function () {

                        queue.forEach(function (onLoginFunction, index) {

                            setTimeout(onLoginFunction, index*100);

                        });
                    },


                    login: function (username, password, callback, onerror) {

                        var onLoginExecute = this.onLoginExecute;

                        var authObject = {
                            username: username,
                            password: password
                        };

                        util.debug.log("Attempting to authenticate");

                        server.request('auth', authObject, function (response) {



                            if(response.status == 200) {

                                util.debug.log("Login Successful");


                                if(!response.data.token)
                                    return "Error";



                                authenticator.setAuthCredentials({
                                    username: username,
                                    token: response.data.token,
                                    permissions: response.data.permissions
                                });

                                loggedIn = true;

                                onLoginExecute();

                                callback();

                            }

                        }, function (response) {

                            util.debug.warn("A problem occurred while logging in");




                            switch (response.status) {
                                case 401:
                                    onerror(response.data);
                                    break;
                                default:
                                    onerror('A problem occurred while trying to contact the server');
                                    break;
                            }
                        });


                    },

                    logoutQueue: [],

                    onLogout: function (onLoginFunction) {

                            this.logoutQueue.push(onLoginFunction);
                    },

                    onLogoutExecute: function () {

                        this.logoutQueue.forEach(function (onLoginFunction, index) {

                            onLoginFunction();


                        });
                    },


    logout: function () {
                        loggedIn = false;
                        authenticator.clearCredentials();


                        this.onLogoutExecute();
                    }

};

  login.session = {

      exists:function(){

          return (authenticator.getAuthCredentials());

      },

      signIn: function(callback, onerror){

          server.signedRequest('validate', {}, function(response){

              if(response.status==200){

                  loggedIn = true;

                  login.onLoginExecute();
                  callback();

                  util.debug.log('Login Validating');
              }

          }, function(response){


              if(response.status==412){

                  util.debug.warn('token expired');
              }else{
                  util.debug.warn('A problem occurred while trying to log in. Status: ' + response.status + '  Message: ' + response.data);
                  onerror();
              }

              login.logout();

          });
      }


  };

        return login;

    });