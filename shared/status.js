/*====================================================================================================================================

Project: dot Learn

Application: HTML Web app

Version: v0.6.0

Author: Sam Bhattacharyya

File: status.js

Description: AngularJS module for issuing notifications and warnings to the main window.

=======================================================================================================================================*/

angular.module('status', []).

factory('statusHandler', function () {

	return {
		
		 notifications: 0,
		 
	
	    message: {
	    	
	    	  type: 'success',
	    	  msg: ''
	    },
	    
	    
	    warn: function (msg){
	    	
	    	   this.message.type = 'danger';
	    	   this.message.msg = msg;
	    	   util.debug.warn(msg);
	    	   this.notifications++;
	    },
	    
	    
	    
	    notify: function (msg) {
	    
	    	   this.message.type = 'success';
	    	   this.message.msg = msg;
	    	   util.debug.info(msg);
	    	   this.notifications++;
	    
	    }  
	    
	 
	
	
	
	};

}).controller('statusController', function ($scope, statusHandler, $timeout) {


  $scope.status = statusHandler; 
 
  $scope.alerts = [];  

  
  $scope.close = function () {
     $scope.alerts = [];
  };  

  
  $scope.$watch('status.notifications', function () {
  	
 

   if($scope.status.message.msg !== ''){
   
           util.safeApply($scope, function () {
                   $scope.alerts = [$scope.status.message];
           });           
           
           $timeout(function () {
                 $scope.close();    
           }, 3000);
     
       }
  
  });

});
