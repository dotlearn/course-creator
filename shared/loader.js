/*====================================================================================================================================

Project: dot Learn

Application: HTML Web app

Version: v0.6.1

Author: Sam Bhattachatryya

File: loader.js

Description: AngularJS module for loading information from course and lesson files. It is actually just a wrapper for a zip reader,
providing the functionality of getting files and resources from the zip archive by name, as well as providing error handling functionality


This file is an abstraction interface between our actual application and zip.js. For more information, checkout the documentation for
zip.js

=======================================================================================================================================*/

angular.module('loader', []).
value('blob', {}).
factory('loader', function (blob) {

	return {
	
			load: function (file) {
				
				blob = this.getZip(file);

			
			},

			
		   getZip: function (file) {      // Extract this ZIP from the raw crs files
		          
		           var zipblob;          
		          
		         	try {
							   zipblob = new Blob([file], {"type" : "application/zip" });               // Convert to binary for zip.js			

					  	}  
					  	
					  	catch(e){                                                           // Couldn't build a blob
					  	
					  	           util.debug.warn('Normal blob load failed');
							          window.BlobBuilder = window.BlobBuilder || 
		                         window.WebKitBlobBuilder || 
		                         window.MozBlobBuilder || 
		                         window.MSBlobBuilder;
				                         
						        if(e.name == 'TypeError' && window.BlobBuilder){     // Older browsers that require blob builder to instantiate blobs
						             var bb = new BlobBuilder();
						              bb.append([file]);
						              zipblob = bb.getBlob("application/zip");
						         }
								    else{
								    	util.debug.warn('Browser doesnt support blobs');
								        // x.error.report("Browser doesn't support blobs ")
								    }
							
							}
							 zipblob.name = file.name;
							 		 
					   return zipblob;
					 
					 
				},






		   getEntries: function(callback, onerror){

			   onerror = onerror || function () {};

			   util.debug.log('Getting zip entries', false);


			   var blobReader = new zip.BlobReader(blob);


			   try{
			   zip.createReader(blobReader, function(reader) {    // Open zip file with zip.js

				   reader.getEntries(function (entries) {                              // Get the entries from the zip file



					     callback(entries);
				   });

			   });

			   } catch (e){

				   util.debug.log('Problem while getting zip entries', false);
				   onerror(e);


			   }


		   },
				
            retrieveResource: function (filename, callback, onerror, onprogress) {
            	
		          	util.debug.log('Loading file ' + filename + ' from zip', false);
		  	
		  	         onerror = onerror || function () {};
		  	         onprogress = onprogress || function () {};
                  
			
					   var zipblob = blob;
					  
						
			         var mime = util.mime.ext2mime(filename);			

						var files = [];
						
                   try {					    
					    
								zip.createReader(new zip.BlobReader(zipblob), function(reader) {    // Open zip file with zip.js
								
							
						   
									reader.getEntries(function(entries) {                              // Get the entries from the zip file
									
									
											entries.forEach(function(entry){
								
														files[entry.filename] = entry;});
														
													
														
											          if(files[filename]){                                     // Check if the filename exists
											          	
											          	
											          	 var file = files[filename];
											          	 
											          	  file.getData(new zip.BlobWriter(mime), function(data){  
											          	  
											          	  	
											          	       util.debug.log('obtained file from zip', false);
											          	       callback(data);
											          	        	
											          	  
											             }, function (progressIndex, totalIndex) {
											             	
				
											             	
											             	        onprogress(progressIndex, totalIndex);
											  
											             
											             });
											          } else{
											          	       util.debug.log('An error occurred while loading the file - it seems that file ' + filename + ' is not in the directory', false); 
					                                     onerror({message: 'error-no-file'});					                                    
											          }
								
								
						        	});
						
					       	});
					       	
					     } catch (e) {
					     	   	util.debug.log('An error occurred while loading the file. Error details ' + e, false); 		             	
		             	       onerror(e);

	             	
					   }

		},
		
		setBlob: function (inputBlob) {
			
			blob = inputBlob;
		},
		
		getBlob: function () {
		
		  return blob;
		
		}
	
	
	
	 };

});

