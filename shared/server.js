/**
 * Created by sam on 1/23/15.
 */
angular.module('server', ['auth']).
    value('queue', []).
    value('database', 'http://lpaasbackend-teachx.rhcloud.com/').     //use single quotes for testing purposes
    value('ready', true).
    factory('server', function($http, authenticator, queue, ready, database){

        return {



            key: "",

            storage: {},


            setClient: function (client) {
                this.key = client.key;
                this.storage = client.storage;
            },


            serverCall: function () {

            },


            request: function(service, data, success, error){


                var tries = 0;

                if(this.key) data.client_id = this.key;

                success = success || function(){};
                error = error || function(){};

                var target= database + service;

                var toSend = function () {

                   util.debug.log('Starting service call to ' + service);


                    $http.post(target , data).success(function(data, status, headers, config){

                        util.debug.log('Server call  to ' + service + ' successful');
                        success({
                            data: data,
                            status: status,
                            headers:headers,
                            config:config
                        });
                    })
                        .error(function(data, status, headers, config) {

                            util.debug.warn('Error occurred while making server call  to ' + service + '. Details: ' + data);


                            var onError = function () {
                                error({
                                    data: data,
                                    status: status,
                                    headers: headers,
                                    config: config
                                });
                            };


                            if (status === 0)                                                          // If server unreachable, try again
                                tryagain(onError);
                            else
                                onError();

                        });
                };

                var tryagain = function (callback) {                             // If there are problems reaching the server, try again a few times

                    if(tries < 5) {
                        util.debug.warn('Failed server call to ' + service +' ' +( tries+1) + 'times');
                        tries++;

                        setTimeout(function () {
                            toSend();
                        }, tries * 1000);

                    } else{

                        util.debug.warn("Unable to reach server - failed: " + service);
                        callback();
                    }

                };


                toSend();


            },

            signedRequest: function(service, data, success, error){

               var credentials =  authenticator.getAuthCredentials();

                if(credentials) {
                    data.token = credentials.token;


                    this.request(service, data, success, error);
                }

            },


            uploadFile: function(file, loc, key, callback, error, onprogress){

                var client_id = this.key;
                var storage = this.storage;

                onprogress = onprogress || function () {};


                this.signedRequest('authFileUpload', {}, function(response){



                    if(typeof response.data.s3Key == 'undefined')
                                    error();

                        var formData = new FormData();


                       console.log(client_id+ "/"+ loc + key);

                        formData.append('key',  client_id+ "/"+ loc + key);
                        formData.append('AWSAccessKeyId', response.data.s3Key);
                        formData.append('Content-Type', file.type);
                        formData.append('policy', response.data.s3Policy);
                        formData.append('signature', response.data.s3Signature);
                        formData.append('acl','public-read');
                        formData.append('file', file);

                        var xhr = new XMLHttpRequest();

                        xhr.onerror = function (e) {
                            error(e);
                        };

                        xhr.onload = function (e) {

                            var status = e.target.status;
                            var response = e.target.responseText;

                            switch(status){
                                case 403:
                                    failure(status, response);
                                    break;
                                case 204:
                                    success();
                                    break;
                                default:
                                    failure(status, response);
                                    break;
                            }
                        };

                       xhr.upload.addEventListener("progress", function(e) {
                           if (e.lengthComputable) {

                               var progress = Math.floor((e.loaded/ e.total)*100);

                               onprogress(progress);

                           }
                       }, false);


                        xhr.open('POST', storage.base , true);
                        xhr.send(formData);

                        function success() {
                            callback();
                        }


                        function failure(status, response){

                            util.debug.log("File upload failed");
                            util.debug.log(status);
                            util.debug.log(response);
                            error(e);
                        }




                    }, function (response) {


                    error(response);

                    });

            }

        };

    });

