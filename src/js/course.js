/*====================================================================================================================================

Project: dot Learn

Application: HTML Web app

Version: v0.6.0

Author: Sam Bhattacharyya

File: workspace.js

Description: AngularJS module which defines the structures of new courses and e-lessons
=======================================================================================================================================*/

angular.module('course', ['auth']).

factory('courseManager', function (authenticator) {


		var create = {
			course: function () {
				var id = util.genID();

				return {

					json: {
						name: (function () {
							return t("lesson.title");
						})(),
						description: (function () {
							return t("lesson.description");
						})(),
						image: 'icon-256.png',
						version: '.6',
						id: id,
						lessons: []
					},

					files: {

						meta: {}

					},

					id: id

				};

			},

			lesson: function () {

				var id = util.genID();




				return {

					json: {
						name: (function () {
							return t("lesson.title");
						})(),
						description: (function () {
							return t("lesson.description");
						})(),
                        author: authenticator.getAuthCredentials().username,
                       copyright: 'cc',
                        language: i18n.lng(),
						image: 'white-blue.png',
						id: id,
						version: '.6',
						items: []
					},

					files: {

						meta: {},

						modules: {},

						res: {}

					},


					id: id,


					updated: Date.now()

				};

			},

			item: function(){

				var id = util.genID();

				return {
					module: '',
					data: '',
					label: '',
					id: id

				};

			}


		};


		var add = {

			lesson: function (object) {

				if(typeof object.json !== 'undefined'){

					if(typeof object.json.lessons !== 'undefined'){

						object.json.lessons.push(this.create.lesson());
					}

				}

			},

			item: function (object, defaultStructure) {


				defaultStructure = defaultStructure || {};

				if(typeof object.json !== 'undefined'){

					if(typeof object.json.items !== 'undefined'){

						if (typeof defaultStructure.module == 'undefined') {
							object.json.items.push(create.item());
						} else{
							object.json.items.push(defaultStructure);
						}

					}

				}


			}


		};


		var reOrder = {

			items: function(itemsObject, newOrder){

					var carbonCopy = util.strip(itemsObject);

				   var reOrdered =[];

				for (var i=0; i < newOrder.length;  i++){

					reOrdered[i] = carbonCopy[newOrder[i]];

				}
				return reOrdered;

			}

		};



		return {
						create: create,
						add:  add,
						reOrder: reOrder
					};



	});



