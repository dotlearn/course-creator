/*====================================================================================================================================

Project: dot Learn

Application: HTML Web app

Version: v0.6.0

Author: Sam Bhattacharyya

File: workspace.js

Description: AngularJS module which handles the editing in the edit panel of the workspace object.

=======================================================================================================================================*/

angular.module('edit', ['angular-storage']).
value('defaultImage', 'http://dashboard.teachx.io.s3-website-us-west-2.amazonaws.com/edit/icons/black-logo.png' ).
    value('backupImage', 'http://dashboard.teachx.io.s3-website-us-west-2.amazonaws.com/edit/icons/black-logo.png' ).
value('fields', ['author', 'copyright', 'language', 'subjects']).
config( [
    '$compileProvider',
    function( $compileProvider )
    {  $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|file|blob):|data:image\//); }
]).

factory('editor', function (defaultImage, fields, backupImage, store) {

	return {

			object: {},

			fields:fields,
						
			image: {
					blob: {},
					url: defaultImage
			},

			set: function (object) {


                try{

                    var platform = store.get('platform');

                    defaultImage = 'http://dashboard.teachx.io.s3-website-us-west-2.amazonaws.com/edit/icons/black-logo.png';

                    console.log('default image: ' + defaultImage );

                    this.image = {
                        blob: {},
                        url: defaultImage
                    };


                    var language;

                    if(i18n.lng().split('-').length > 1)  language = i18n.lng().split('-')[0];
                    else language = i18n.lng();

                    object.json.language = language;

                    object.json.subjects = sessionStorage.categoryName || "";
                    object.json.image = defaultImage;


                } catch(e){

                    defaultImage = backupImage;

                    this.image={
                        blob: {},
                        url:  backupImage
                    };


                }
			   


                this.object =object;

            },
			
			edit: function (object) {



			        this.object = object;


                     if(object.files.meta[object.json.image]){

                         var blob = object.files.meta[object.json.image];
                         var url = URL.createObjectURL(blob);

                         this.image = {
                             blob: blob,
                             url: url
                         };

                     } else{


                         var platform = store.get('platform');

                         if(platform){

                             defaultImage = "http://webapp.teachx.io/" + platform.key + "/var/solid-logo.png";

                             console.log('default image: ' + defaultImage );

                             this.image = {
                                 blob: {},
                                 url: defaultImage
                             };


                             object.json.image = defaultImage;


                         }else {


                             console.log('no platform');

                             this.image = {
                                 blob: {},
                                 url: backupImage
                             };


                         }

                     }

			},

        languages: {
            "af": "Afrikaans",
            "am": "አማርኛ",
            "ar": "العربية",
            "as": "অসমীয়া",
            "bn": "বাংলা",
            "cz": "čeština",
            "de": "Deutsch",
            "el": "ελληνικά",
            "en": "English",
            "es": "Español",
            "fa": "فارسی",
             "ff": "Pulaar",
            "fr": "Français",
            "gu": "ગુજરાતી",
            "ha": "هَوُسَ",
             "he": "עברית",
             "hi": 'हिंदी',
            "hu": "magyar",
            "id": "Bahasa Indonesia",
            "it": "italiano",
            "ja": "日本語",
            "jv": "basa Jawa",
            "kk": "қазақ тілі",
            "km": "ខេមរភាសារ",
            "kn": "ಕನ್ನಡ",
            "ko": '한국어',
            "lo": "ພາສາລາວ",
            "mg": "fiteny malagasy",
            "ml": "മലയാളം",
            "mr": "मराठी",
            "ms": "bahasa Melayu",
            "my": "ဗမာစာ",
            "ne": "नेपाली",
            "nl": "Nederlands",
            "or": "ଓଡ଼ିଆ",
            "pa":  "ਪੰਜਾਬੀ",
            "pl": "język polski",
            "ps": "پښتو",
            "pt": 'português',
            "qu": "Runa Simi",
            "ro": "limba română",
            "ru": "Русский",
            "sd": "سنڌي",
            "si": "සිංහල",
            "sw": "Kiswahili",
            "ta": "தமிழ்",
            "te": "తెలుగు",
            "th": "ไทย",
            "tl": "Wikang Tagalog",
            "tr": "Türkçe",
            "uk": "країнська мова",
            "ur": "اردو",
            "uz": "Oʻzbek",
            "vi": "Việt Nam",
            "yo": "Èdè Yorùbá",
            "zh": "中文"
        }
     };
});