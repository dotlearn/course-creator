/*====================================================================================================================================

Project: dot Learn

Application: HTML Web app

Version: v0.6.0

Author: Sam Bhattacharyya

File: packager.js

Description: This module provides the import/export functionality for the Course Creator software

=======================================================================================================================================*/

angular.module('packager', ['file', 'course', 'server', 'auth', 'version']).
config( [
    '$compileProvider',
    function( $compileProvider )
    {   
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|blob|chrome-extension):/);
        // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
    }
]).

factory('packager', function (loader, courseManager, server, fileHandler, authenticator, versionManager) {

	return {

		fromServer: function(lessonID, platform, success, error, progress){


			error = error || function(){};
			progress = progress || function(){};
			var packager = this;



				var url = platform.storage.base + platform.key + '/' +  platform.storage.lessons + lessonID + '.lsn';
				console.log(url);

				util.debug.log('Downloading file from the server');


				download(url, function(file){

					util.debug.log('Downloaded lesson file from the server');

					util.debug.log('Opening  / Importing the file');


					openFile(file, success, error, onprogress);



					function onprogress(pEvent){
						// Assume 30 percent of the progress comes from the file extraction
						progress(Math.floor(pEvent *0.3 + 70));
					}

				});



			function download(url, callback){


				var xhr = new XMLHttpRequest();

				xhr.open("GET",url, true);

				xhr.responseType = "arraybuffer";

				xhr.onload = function(oEvent) {

					var blob = new Blob([oEvent.target.response], {type: 'application/zip'});
					blob.name = lessonID+ '.lsn';

					callback(blob);

				};

				xhr.onprogress = function (oEvent) {

					if (oEvent.lengthComputable)
						progress(Math.round((oEvent.loaded / oEvent.total)*80));

				};

				xhr.onerror = function (oEvent) {
					error(oEvent);
				};

				xhr.send();


			}



			function openFile(file, callback, onerror, onprogress){

				fileHandler.open(file, function (){

					util.debug.log('Downloaded file successfully opened');
					packager.import(fileHandler.getJSON(), fileHandler.type, callback, onerror, onprogress);

				}, onerror);
			}



		},


		toServer: function (object, callback, onerror, update) {

			update = update || function(){};
			onerror = onerror || function(){};


			var packager= this;

			var size;


			function getImage(callback){


				if(!object.json.image) return callback("No image present!");

				if(object.files.meta[object.json.image]) return callback();
				else{

				var url= 'icons/black-logo.png';
/*					var isWeb =  new RegExp("^http?://");




					console.log(object.json.image);


					if(isWeb.test(object.json.image)) url = object.json.image;
*/

					var name = url.split('/').pop();


					console.log(name);

					object.json.image = name;

					console.log(object.json.image);


					console.log("Getting url " + url);


					var xhr = new XMLHttpRequest();

					xhr.open("GET",url, true);

					xhr.responseType = "arraybuffer";

					xhr.onload = function(oEvent) {


						console.log("Got image");
						var blob = new Blob([oEvent.target.response], {type: 'image/png'});
						blob.name = name;
						object.files.meta[name] = blob;

						console.log(object.files.meta[name]);

						callback();



					};

					xhr.onerror = function (oEvent) {
						return callback(oEvent);
					};

					xhr.send();





				}


			}




			function getBlob(callback){


				util.debug.log('Exporting lesson file for publishing');


				packager.export(object, 'lesson', function(url, blob){

					util.debug.log('Lesson file successfully published');

					size = blob.size;

					callback(null, blob);

				}, function(progress){

					var overAllProgress = Math.floor(0.2*progress);

					update(overAllProgress);



				}, function (e) {
					callback("Error packaging for export " + e );
				});


			}




			function uploadFile(file, callback){


				util.debug.log('Uploading lesson file to the server');


				server.uploadFile(file, 'lessons/', object.json.id + '.lsn', function(){


					util.debug.log('Lesson file successfully uploaded');

					callback();

				}, function(e){

					callback("Error uploading lesson file " + e);

				}, function(progress){

					var overAllProgress = Math.floor(0.6*progress + 20);


					update(overAllProgress);
				});






			}

			function uploadImage(callback){


				util.debug.log('Uploading lesson file to the server');


				   var imageFile = object.files.meta[object.json.image];



				console.log(imageFile);

				  if(imageFile){

					  var ext = object.json.image.split('.')[1];

					  server.uploadFile(imageFile, 'icons/', object.json.id +'.' +  ext, function(){

						  util.debug.log('Uploaded lesson file to server');

						  callback();

					  }, function(e){

						  callback("Error uploading Image File: " +e);

					  }, function(progress){

						  var overAllProgress = Math.floor(0.1*progress + 80);


						  update(overAllProgress);

					  });

				  } else{

					  util.debug.log('No image file found, continuing with upload');
					  callback();
				  }

			}

			function updateDB(callback){

				util.debug.log("Updating the lesson on the database");


				var lesson = JSON.parse(JSON.stringify(object.json));

				lesson.version = versionManager.getCurrent();

				if(object.json.image) lesson.image = object.json.id + '.' + object.json.image.split('.')[1];

				var category = sessionStorage.category;

				if(category) lesson.category = category;

			   if(size) lesson.filesize= size;

				lesson.uploader = authenticator.getAuthCredentials().username;

				server.signedRequest('setLesson', {lesson: lesson}, function (data) {

					util.debug.log("Lesson successfully updated");

					update(100);

					callback();
				}, function(e){
					callback("An error ocurred while updating the DB " + e);
				});

			}


			async.waterfall([getImage, getBlob, uploadFile, uploadImage, updateDB], function(err){

				if(err) {
					util.debug.warn("An error ocurred while publishing the lesson: " + err);
					onerror(err);
				} else{

					callback();
				}



			});



		},



  			
  		export: function (object, type, callback, update, onerror) {
  			   		   	 

			onerror = onerror || function () {};
			update = update || function(){};
  	
			var lesson = object;



			lesson.json.version = versionManager.exportJSON();

            var size = 0;               

            var x = lesson.size;

            var filenames = [];
            var files = [];
            
            
            var packager = this;


			// ----------------------------- Here you package the meta files


				Object.keys(lesson.files.meta).forEach(function (key) {
					
				if (typeof key !== 'undefined') {
				   filenames.push('meta/'+key);
				   files.push(lesson.files.meta[key]);
                  size = size+lesson.files.meta[key].size;
				   		   	}
				});


			// ----------------------------- Here you package the res files
				Object.keys(lesson.files.res).forEach(function (item) {


					if(typeof item !== 'undefined'){

						Object.keys(lesson.files.res[item]).forEach(function (key) {

							if (typeof key != 'undefined') {
								filenames.push('res/'+item + "/" + key);
								files.push(lesson.files.res[item][key]);
								size = size+lesson.files.res[item][key].size;
							}

						});

					}

				});



			// ----------------------------- Here you package the module files ------------------



			Object.keys(lesson.files.modules).forEach(function (module) {


				if(typeof module!== 'undefined'){

					Object.keys(lesson.files.modules[module]).forEach(function (key) {

						if (typeof key != 'undefined') {
							filenames.push('res/'+module+ "/" + key);
							files.push(lesson.files.modules[module][key]);
							size = size+lesson.files.modules[modules][key].size;
						}

					});

				}

			});
				var i = 0;
				
				this.clear();

       
           var filename;

           switch(type) {


                 case 'course':
                    filename = 'course.json';
                    break;
                 case 'e-lesson':
                    filename = 'lesson.json';
                    break;

			   case 'lesson':
				   filename = 'lesson.json';
				   break;

			   default:
				   filename = 'lesson.json';
				   break;
           }
           
           var lastTotal = 0;
           var currentTotal = 0;
  
           util.debug.log('Initiating zip creation', false);  
  
  		   	zip.createWriter(new zip.BlobWriter("application/x-lrn"), function(writer) {

				util.debug.log('Created zip file Object', false);



				writer.add(filename , new zip.TextReader(JSON.stringify(lesson.json)), function() {
                    
                      util.debug.log('Adding the JSON file', false);  
	           
	                     var next = function () {
	                     	
	                     	 util.debug.log('Adding file ' + filenames[i], false);  

               	      	 writer.add(filenames[i], new zip.BlobReader(files[i]), function(){     
               	      
               	      	    packager.message = t("packager.writing", {progress:(i+1), length: files.length});
               	      	   //packager.message = 'Writing item ' + (i+1) + 'out of ' + files.length;
               	      	    i++;
               	      	    if(i<files.length){
               	      	    	  
               	      	    	 next();
               	      	    	
               	      	    }  else{
               	      	           
               	      	           writer.close(function (blob) {
         
               	      	              var url = URL.createObjectURL(blob);
               	      	             
               	      	      
               	      	              
               	      	              packager.url = url;
               	      	              packager.blob = blob;

									   packager.message = t("packager.done");
               	      	              //packager.message = 'Done';
               	      	              
               	      	              callback(url, blob);
               	      	           });
               	      	       
               	      	       }
               	      	    
               	      	    },  function(currentIndex, totalIndex) { 
               	      	    
               	      	           if(currentIndex > lastTotal){
                                      currentTotal = currentTotal + currentIndex - lastTotal;
                                      }
                                      lastTotal = currentIndex;  
                                      
                                      
       
                                     update(currentTotal/size*100); 	      	    
               	      	    });
               	      	   
               	      	 };
               	      
               	         next();
	                     	
	                     });
                       	
	                  
   		   	}, function (message) {


				               onerror(message);
               	      	    
               	      	    });
  		          
  		},
  		
  		
  		import: function (object, type, callback, onerror, onprogress) {
   	
        var size = loader.getBlob().size;
        var completed = 0;
        var lastTotal =0;


			console.log(size);

        util.debug.log('Starting the import stage...');
        
        onprogress = onprogress || function (){};        
        onerror = onerror || function () {};



			var ob = courseManager.create.lesson();
    		ob.json = object;
			ob.id = object.id;


			loader.getEntries(function (entries) {




				var imported = 0;
				var toImport= entries.length;

				var totalProgress = 0;
				var percentProgress = 0;


				var done = function () {

					imported++;

					if(toImport == imported){

						util.debug.log("Done importing file");
						callback(ob);
					}
				};



				var load = function (filepath, dest, key) {

					var thisFileProgress = 0;


					loader.retrieveResource(filepath, function(file){


						dest[key] = file;
						done();

					}, onerror, function(progressIndex, totalIndex){


						var progress = progressIndex - thisFileProgress;


						totalProgress += progress;

						if(Math.floor((totalProgress/size)*100) > percentProgress){

							percentProgress =Math.floor((totalProgress/size)*100);
							onprogress(percentProgress);

						}

						thisFileProgress = progressIndex;

					});


				};


				entries.forEach(function (entry) {

					var filepath = entry.filename;

					var dirs = filepath.split("/");

					var filename = dirs[(dirs.length -1)];


					if(dirs.length >1 ){

						switch (dirs[0]){

							case "meta":

								load(filepath, ob.files.meta, filename );

								break;
							case "res":
								 var id = dirs[1];
								if(typeof id == 'undefined'){
									done();
								} else{

									console.log("id ==");
									console.log(id);
									ob.files.res[id] = ob.files.res[id] || {};
									load(filepath, ob.files.res[id], filename);
								}
							    break;

							default:
								done();
								break;
						}

					} else{
						done();
					}

				});

			}, onerror);
        

   
   },
  		
  		
  		
  		progress: 0,
  		
  		
  		
  		clear: function () {
  		
  		    this.blob = {};
  		    this.url = '';
  		    this.message =  '';
  		},
  		
  		blob:{},
  		
  		url: '',
  		
  		getURL: function () {
  		   return this.url;
  		},
  		 
  		message: ''
              		
  		
  		};
  	
	


});

