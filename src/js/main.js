/*  dot Learn Course Creator Web App
    Filename: load.js
 	  Author: Sam Bhattacharyya
 		 Description: Uses Load.js to load the required Javascript assets								
*/

//                            Load Required Assets    
//-----------------------------------------------------------------------------------------------


Modernizr.load([
{ 
 test: Modernizr.indexeddb,
 nope: 'js/lib/IndexedDBShim.min.js'

}
]);





LazyLoad.js(['js/lib/jquery/jquery.js', 'js/lib/angular/angular.min.js', 'js/util.js', 'js/app.js', 'js/lib/zip/zip.js','js/lib/zip/zip-ext.js', 'js/course.js', 'js/edit.js', 'js/workspace.js', 'js/store.js', 'js/library.js', 'js/item.js', 'js/packager.js', 'js/file.js', 'js/lib/bootstrap/ui-bootstrap.js', 'js/status.js', 'js/loader.js', 'js/module.js', 'js/dotlearn.js', 'js/dotlearn-ui.js', 'js/modules-ui.js', 'js/debug.js', 'js/auth.js', 'js/login.js', 'js/server.js' ,  'js/lib/jquery/jquery-ui.js', 'js/lib/angular-storage.js', 'js/lib/aes.js', 'js/lib/async.min.js','js/version.js', 'js/lib/il8n.js']  , function(){  //Vendor
		

    jQuery(".panel").draggable({containment: 'window'});

    zip.workerScriptsPath = "js/lib/zip/";

});
