/*====================================================================================================================================

Project: dot Learn

Application: HTML Web app

Version: v0.6.0

Author: Sam Bhattacharyya

File: workspace.js

Description: AngularJS module which handles the store of courses and lessons in the object store (IndexedDB). Previous e-courses and files
can be opened and edited from the library, and new courses can be saved to the Library store
=======================================================================================================================================*/

angular.module('library', ['store']).
value('fields', ['author', 'copyright', 'language', 'subjects', 'pre-requisites']).
factory('library', function (dBstore, fields, workspace) {

	return {

         courses: [],
         
         fields: fields,
         
         lessons: [],
         
         previews: [],
         
         type: '',
         
         getLessons: function (){
              
         },
         
         getCourses: function (type, callback) {
         	
         	callback = callback || function () {};

             this.type = type;

            var handler = this;
            dBstore.list(type, function (courses) {
                handler.courses = courses; 
                handler.getPreviews();   
                handler.type = type;
           
                callback(courses);
            });         	
         	
         },
         
         remove: function (index, callback, onerror) {
             
                 callback = callback || function () {};    
                 onerror =  onerror || function () {};         	
         	
                var type = this.type;
                
                var id = this.courses[index].id;
                
                dBstore.remove(type, id, callback, onerror);
         
         
         },
         
         deleteAll: function (callback, onerror) {
         	
						onerror = onerror || function () {};
						callback = callback || function () {};             
             
             if (this.type !== '') {
              dBstore.deleteAll(this.type, callback, onerror);
             }
         
         },
         
         getPreviews: function () {
         	
         	
               var courses = this.courses;
               var i = 0;
               var l = courses.length;
		         var previews = this.previews;
		       
		       
		        courses.forEach(function (course) {
                 		        
		               var im = course.json.image;
		               var blob = course.files.meta[im];
		              
		              try{ 
		               var url = URL.createObjectURL(blob); 
		                previews.push(url);  
		               } catch (e) {
		               previews.push('icons/icon-120.png');
		            }
		        });

         }       
        
     };
        

});