
/*====================================================================================================================================

Project: dot Learn

Application: HTML Web app

Version: v0.6.0

Author: Sam Bhattacharyya

File: workspace.js

Description: AngularJS module for handling the module management and item editing of the items in each course to be edited
=======================================================================================================================================*/

angular.module('item', ['workspace', 'module', 'status']).
	factory('itemManager', function (workspace, moduleManager) {

		return {

			previewPanel: document.getElementById('preview-frame-container'),

			editPanel: document.getElementById('editor-frame-container'),


			current: {

				item: {

					live: {},

					model: {}

				},

				frame: {

					preview: {},

					edit: {}

				}

			},

			getLabel: function(){

				if(typeof this.current.item.live.label == 'undefined'){
					return '';
				} else{

					return this.current.item.live.label;
				}

			},

			setLabel: function (label) {

				this.current.item.live.label = label;
			},


			previewItem: function () {


				var itemObject = util.strip(this.current.item.live);

				var modules = moduleManager.getModules();
				$(this.previewPanel).empty();


				var iframe = document.createElement('iframe');
				iframe.id = 'preview';
				iframe.className = 'preview';
                iframe.setAttribute('allowFullScreen', '');

				iframe.src="modules/"+itemObject.module + "/" + modules[itemObject.module].main;

				this.current.frame.preview = iframe;

				var viewObject = this.createViewObject(itemObject);

				iframe.onload = function () {
					iframe.contentWindow.onLoad(viewObject);
				};

				this.previewPanel.appendChild(iframe);



			},


			editItem: function(item){

				this.current.item.live = item || this.current.item.live;

				var itemObject = util.strip(this.current.item.live);

				this.current.item.model = itemObject;

				var modules = moduleManager.getModules();



				$(this.editPanel).empty();

				var iframe = document.createElement('iframe');
				iframe.id = 'editor';
				iframe.className = 'editor';
				iframe.frameBorder=0;

                iframe.setAttribute('allowFullScreen', '');

				iframe.src="modules/"+itemObject.module + "/" + modules[itemObject.module].edit.main;

				this.current.frame.edit = iframe;

				var editObject = this.createEditObject(itemObject);

				iframe.onload = function () {

					iframe.contentWindow.onLoad(editObject);
				};

				this.editPanel.appendChild(iframe);

			},

			saveItem: function () {

				var iframe = this.current.frame.edit;

				var data = iframe.contentWindow.onFinished();

				this.current.item.live.data = util.strip(data);

				$(this.editPanel).empty();

			},


			createViewObject: function (item) {

				return {

					file: {


						get: function(fileName, callback){

							callback(workspace.getResourceByItem(item.id, fileName));
						},


						inModule: function(fileName, callback){

							callback(workspace.getResourceByModule(item.module, fileName));


						}


					},

					data: item.data,


					lang: i18n.lng(),


					blocker: {
						block: function(){

						},

						unblock: function(){

						}
					},

					user: {

						get: function(){

							return {};

						},

						set: function(object){

						}
					}
				};
			},



			createEditObject: function(item){


				return {


					file: {

						item: {

							get: function(fileName){

								return workspace.getResourceByItem(item.id, fileName);
							},

							set: function (key, fileObject) {

								workspace.setResourceByItem(item.id, key, fileObject);
							},

							remove: function (fileName) {
								workspace.removeResourceByItem(item.id, fileName);
							}

						},


						module:{

							get: function(fileName){

								return workspace.getResourceByModule(item.module, fileName);
							},

							set: function (key, fileObject) {

								workspace.setResourceByModule(item.module, key, fileObject);
							},

							remove: function (fileName) {
								workspace.removeResourceByModule(item.module, fileName);
							}




						},

						shared: {



						}


					},


					data: item.data
				};
			}

		};
	});


