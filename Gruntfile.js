var fs = require('fs');
var exec= require('child_process').exec;
var spawn= require('child_process').spawn;
var StringDecoder = require('string_decoder').StringDecoder;
var async = require('async');


module.exports = function (grunt) {

	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-sync');
	grunt.loadNpmTasks('grunt-rename');
	grunt.loadNpmTasks('grunt-aws-s3');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-jsonlint');
	grunt.loadNpmTasks('grunt-karma');
	grunt.loadNpmTasks('grunt-open');

	grunt.initConfig({

		aws_s3: {
			options: {
				accessKeyId: process.env.AWS_ACCESS_KEY_ID, // Use the variables
				secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY, // You can also use env variables
				region: 'us-west-2',
				uploadConcurrency: 5, // 5 simultaneous uploads
				downloadConcurrency: 5 // 5 simultaneous downloads
			},
			production: {
				options: {
					bucket: 'dashboard.teachx.io',
					differential: true // Only uploads the files that have changed
				},
				files: [
					{expand: true,
						cwd: '../LPaaS/push/',
						src: ['**', '!**/*~'],
						dest: '/'}
				]
			},

			staging: {
				options: {
					bucket: 'staging-dashboard.teachx.io',
					differential: true // Only uploads the files that have changed
				},
				files: [
					{expand: true,
						cwd: '../LPaaS/push/',
						src: ['**', '!**/*~'],
						dest: '/'}
				]
			}
		},

		open : {
			dev: {
				path: 'http://localhost:63342/LPaaS/test/index.html',
				app: 'google-chrome-stable'
			}
		},


		karma: {
			unit: {
				configFile: 'karma.conf.js'
			}
		},


		jsonlint: {
			il8n: {
				src: ["il8n/locales/**" ],
				filter: 'isFile'
			}
		},

		clean: {

			build: ['build'],
			dist: ['dist'],
			push: ['../LPaaS/push'],
			test: ['../Course Creator/test'],
			pushTest: ['../LPaaS/test']
		},


		sync: {

			client: {

				files: [{
					cwd: 'shared/',
					src: ['**', '!**~'],
					dest: '../shared/',
				}],
				pretend: false, // Don't do any IO. Before you run the task with `updateAndDelete` PLEASE MAKE SURE it doesn't remove too much.
				verbose: true // Display log messages when copying files
			},

			repo: {
				files: [{
					cwd: '../shared/',
					src: ['**', '!**/*~'],
					dest: 'shared/',
				}],
				pretend: false, // Don't do any IO. Before you run the task with `updateAndDelete` PLEASE MAKE SURE it doesn't remove too much.
				verbose: true // Display log messages when copying files
			}


		},


		copy: {

			main: {
				files: [
					{
						expand: true,
						cwd: 'src/',
						src: ['**', '!**/*~'],
						dest: 'dist/web/',
						filter: 'isFile'
					}
				]

			},

			pushLPaaS: {

				files: [
					{
						expand: true,
						cwd: '../LPaaS/dist/',
						src: ['**', '!**/*~'],
						dest: '../LPaaS/push/',
						filter: 'isFile'
					}
				]

			},

			test: {

				files: [
					{
						expand: true,
						cwd: '../Course Creator/dist/',
						src: ['**', '!**/*~'],
						dest: '../Course Creator/test/',
						filter: 'isFile'
					}
				]

			},


			pushTest: {

				files: [
					{
						expand: true,
						cwd: '../Course Creator/test/',
						src: ['**', '!**/*~'],
						dest: '../LPaaS/test/edit/',
						filter: 'isFile'
					}
				]

			},


			pushTestLPaaS: {

				files: [
					{
						expand: true,
						cwd: '../LPaaS/dist/',
						src: ['**', '!**/*~'],
						dest: '../LPaaS/test/',
						filter: 'isFile'
					}
				]

			},




			pushEditor: {

				files: [
					{
						expand: true,
						cwd: '../Course Creator/dist/',
						src: ['**', '!**/*~'],
						dest: '../LPaaS/push/edit/',
						filter: 'isFile'
					}
				]

			},


			build: {
				files: [
					{
						expand: true,
						cwd: 'src/',
						src: ['**', '!**/*~'],
						dest: 'build/base/',
						filter: 'isFile'
					}
				]

			},

			shared: {
				files: [
					{
						expand: true,
						cwd: 'shared/',
						src: ['**', '!**/*~'],
						dest: 'build/base/js/',
						filter: 'isFile'
					}
				]

			},

			il8n: {
				files: [
					{
						expand: true,
						cwd: 'il8n/locales/',
						src: ['**', '!**/*~'],
						dest: 'build/base/locales/',
						filter: 'isFile'
					}
				]

			},


			premin: {
				files: [
					{
						expand: true,
						cwd: 'build/base',
						src: ['**', '!**/*~'],
						dest: 'build/min/',
						filter: 'isFile'
					}
				]
			},

			postmin: {
				files: [
					{
						expand: true,
						cwd: 'build/min',
						src: ['**', '!**/*~'],
						dest: 'dist/',
						filter: 'isFile'
					}
				]
			},



			modules: {
				files: [
					{
						expand: true,
						cwd: '../modules/',
						src: ['**', '!**/*~'],
						dest: 'build/base/modules/',
						filter: 'isFile'
					}



				]




			}

		},

		rename: {

			lesson: {
				src: '../LPaaS/push/edit/index.html',
				dest: '../LPaaS/push/edit/lesson.html'
			}

		},


		jshint: {

			all: ['Gruntfile.js', 'build/base/js/**/*.js', '!build/base/js/lib/**']
		},


		uglify: {

			main: {
				files: [
					{
						expand: true,
						cwd: 'build/min/js/',
						src: '*.js',
						dest: 'build/min/js/'

					}

				]

			},

			options: {
				mangle:false
			}
		}

	});



	grunt.registerTask('setStagingServerLPaaS', 'Sets the server.js file in the local test server to point to the right option', function () {


		var done = this.async();

		fs.readFile('../LPaaS/test/js/server.js', {encoding: 'utf8'}, function(err, data){

			if(err) return grunt.fail.fatal('error opening server.js: ' + err);




			var newFile = data.replace('http://lpaasbackend-teachx.rhcloud.com/', 'http://stagingbackend-teachx.rhcloud.com/');


			fs.writeFile('../LPaaS/test/js/server.js', newFile, function (err) {


				if(err) return grunt.fail.fatal('error saving edited server.js: ' + err);

				done();

			});




		});


	});



	grunt.registerTask('setLocalServerLPaaS', 'Sets the server.js file in the local test server to point to the right option', function () {


		var done = this.async();

		fs.readFile('../LPaaS/test/js/server.js', {encoding: 'utf8'}, function(err, data){

			if(err) return grunt.fail.fatal('error opening server.js: ' + err);




			var newFile = data.replace('http://lpaasbackend-teachx.rhcloud.com/', 'http://localhost:8000/');


			fs.writeFile('../LPaaS/test/js/server.js', newFile, function (err) {


				if(err) return grunt.fail.fatal('error saving edited server.js: ' + err);

				done();

			});




		});


	});


	grunt.registerTask('setLocalPointer', 'Sets the server.js file in the local test server to point to the right option', function () {


		var done = this.async();

		fs.readFile('../LPaaS/test/js/library.js', {encoding: 'utf8'}, function(err, data){

			if(err) return grunt.fail.fatal('error opening library.js: ' + err);




			var newFile = data.replace('http://dashboard.teachx.io/edit/lesson.html', 'http://localhost:63342/LPaaS/test/edit/index.html');


			fs.writeFile('../LPaaS/test/js/library.js', newFile, function (err) {


				if(err) return grunt.fail.fatal('error saving edited library.js: ' + err);

				done();

			});




		});


	});


	grunt.registerTask('setStagingPointer', 'Sets the server.js file in the local test server to point to the right option', function () {


		var done = this.async();

		fs.readFile('../LPaaS/dist/js/library.js', {encoding: 'utf8'}, function(err, data){

			if(err) return grunt.fail.fatal('error opening library.js: ' + err);




			var newFile = data.replace('http://dashboard.teachx.io/edit/lesson.html', 'http://staging-dashboard.teachx.io/edit/lesson.html');


			fs.writeFile('../LPaaS/dist/js/library.js', newFile, function (err) {


				if(err) return grunt.fail.fatal('error saving edited library.js: ' + err);

				done();

			});




		});


	});






	grunt.registerTask('setStagingServer', 'Sets the server.js file in the local test server to point to the right option', function () {


		var done = this.async();

		fs.readFile('test/js/server.js', {encoding: 'utf8'}, function(err, data){

			if(err) return grunt.fail.fatal('error opening server.js: ' + err);




			var newFile = data.replace('http://lpaasbackend-teachx.rhcloud.com/', 'http://stagingbackend-teachx.rhcloud.com/');


			fs.writeFile('test/js/server.js', newFile, function (err) {


				if(err) return grunt.fail.fatal('error saving edited server.js: ' + err);

				done();

			});




		});


	});




	grunt.registerTask('setLocalServer', 'Sets the server.js file in the local test server to point to the right option', function () {


		var done = this.async();


		var files = ['test/js/server.js', 'test/modules/audio-slideshow/edit/lib/app.js'];



		async.each(files, function(file, next){


			fs.readFile(file, {encoding: 'utf8'}, function(err, data){

				if(err) return next('error opening server.js: ' + err);


				var newFile = data.replace('http://lpaasbackend-teachx.rhcloud.com/', 'http://localhost:8000/');


				fs.writeFile(file, newFile, function (err) {


					if(err) return next('error saving edited server.js: ' + err);

					next();

				});

			});



		}, function(err){

			if(err) grunt.fail.fatal(err);

			done();
		});



	});




	grunt.registerTask('localServer', 'runs the local server', function () {


		var done = this.async();

		var server = spawn('node', ['server', 'local'], {maxBuffer: 1024 * 10500, cwd: '../Backend/lpaasbackend/'});


		var decoder = new StringDecoder('utf8');

		server.stdout.on('data', function(data) {



			var output = decoder.write(data);
			grunt.log.write("Server: " + output);


		});


		server.stderr.on('data', function(data) {
			grunt.log.error("A problem ocurred with the server while testing the local buildandroid server "+ decoder.write(data));
			error = true;
		});





	});

	grunt.registerTask('upload', 'Upload to Amazon', function () {



	});



	grunt.registerTask('syncShared',  ['sync:client', 'sync:repo']);

	grunt.registerTask('build', ['clean:build','clean:dist', 'copy:build',  'syncShared', 'copy:shared', 'copy:modules', 'jsonlint:il8n',  'copy:il8n', 'test', 'minify']);

	grunt.registerTask('test', ['jshint', 'clean:test', 'copy:test', 'karma']);

	grunt.registerTask('minify', ['copy:premin', 'uglify', 'copy:postmin']);

	grunt.registerTask('dist', ['copy:pushLPaaS', 'copy:pushEditor', 'rename:lesson']);




	grunt.registerTask('local', ['build', 'clean:test', 'copy:test', 'setLocalServer', 'copy:pushTestLPaaS', 'setLocalServerLPaaS', 'copy:pushTest',  'setLocalPointer', 'open:dev', 'localServer']);

	grunt.registerTask('stagingBackend', ['build', 'clean:test', 'copy:test', 'setStagingServer', 'copy:pushTestLPaaS', 'setStagingServerLPaaS', 'copy:pushTest',  'setLocalPointer',  'open:dev']);

	grunt.registerTask('staging', ['build',  'dist', 'setStagingPointer', 'aws_s3:staging']);

	grunt.registerTask('production', ['build',  'dist', 'aws_s3:production']);



};
