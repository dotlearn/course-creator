/*====================================================================================================================================

Project: dot Learn

Application: HTML Web app

Version: v0.6.0

Author: Sam Bhattacharyya

File: workspace.js

Description: AngularJS module which handles the current object (e-course or e-lesson) being edited in the workspace. Partly reference 
to other objects (to obtain properties of the current object), partly a bridge to the model (UI)

=======================================================================================================================================*/

angular.module('workspace', ['store']).
factory('workspace', function (dBstore) {

	return {

		   current:{

            object: {},

            type: {},

            selected:  {}

           },

			setObject: function (object, type) {
				this.current = this.createWorkspaceObject(object, type);
			},


		createWorkspaceObject: function(object, type){
			return {
				    object: object,
				    type: type,
                    selected: {
                        item: -1,
                        lesson: -1
                    }
			};
		},



       getResourceByItem: function (id, key) {

         if (typeof   this.current.object.files.res[id] == 'undefined') {
          return {};
         } else {
          return this.current.object.files.res[id][key];
         }

        },

        getResourceByModule: function (module, key) {

         return this.current.object.files.modules[module][key];


        },

        getResourceInShared: function () {

        },



        setResourceByItem: function (id, key, resource) {

         console.log("setting key " + key + "for item " + id);


         if (typeof   this.current.object.files.res[id] == 'undefined') {
          this.current.object.files.res[id] = {};
         }


         this.current.object.files.res[id][key] = resource;


        },

        setResourceByModule: function (module, key, resource) {

         console.log("setting key " + key + "for module " + module);

         var meta = this.current.object.files.modules;

         if (typeof meta[module] == 'undefined') {
          meta[module] = {};
         }

         meta[module][key] = resource;

        },

        setResourceInShared: function () {

        },




        removeResourceByItem: function (id, key) {

         if (typeof   this.current.object.files.res[id] == 'undefined') {
          console.log("no item present");
         } else {
          delete this.current.object.files.res[id][key];
         }

        },

        removeResourceByModule: function (module, key) {

         delete this.current.object.files.modules[module][key];

        },

        removeResourceInShared: function () {

        },






        deleteItem: function (index) {

         var item =       this.current.object.json.items.splice(index, 1)[0];
         util.debug.log("deleting item #" + index );
         delete this.current.object.files.res[item.id];

        },



	       deleteObject: function (callback, onerror) {
	       	
	       	      var workspace = this;
	       	      
	       			dBstore.remove(this.current.type, this.current.object.id, function () {
             	       			workspace.current = {};
             	       			callback();
	       			
	       			}, function (event) {                                            // Error 

	       			            onerror(event);
	       			
	       			});
	     
	       },

          save: function (callback, onerror) {

			  callback = callback || function(){};
			  onerror = onerror || function(){};

              this.current.object.updated = Date.now();

              dBstore.set(this.current.type, this.current.object, function () {
                     callback();           
              },
               function () {
                          onerror();
               });
           },
                   
           error: false

	};


});



