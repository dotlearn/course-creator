/*====================================================================================================================================

 Project: dot Learn

 Application: HTML Web app

 Version: v0.6.0

 Author: Sam Bhattacharyya

 File: workspace.js

 Description: The main AngularJS module which contains the main app UI logic and custom directives
 =======================================================================================================================================*/


var app = angular.module('app', ['course', 'edit', 'workspace', 'store', 'library', 'packager', 'server', 'ui.bootstrap', 'status', 'file', 'auth', 'login',  'item', 'module', 'angular-storage']);

app.controller('UIController', function ($scope, courseManager, editor, workspace, library,  packager, statusHandler, login, authenticator, store, dBstore, server) {

    $scope.workspace = workspace;
    $scope.editor = editor;
    $scope.userLanguage = 'en';
    $scope.loadingStatus = "loading";




    function setup() {


        async.series([setupLanguage, setupMenu, setupLogin,  setupUIEventHandlers, setPlatform,  setupLesson, setupCategory], function(){





            $scope.loadingStatus = "ready";
            util.safeApply($scope);


        });

    }


    setup();

    function setPlatform(callback){

        $scope.platform = store.get('platform') || {key: '', storage: ''};

        console.log("Setting client");
        console.log('client');
        server.setClient($scope.platform);
        console.log('set client');
        callback();
    }


    function setupCategory(callback){

        if(!sessionStorage) return callback();
        if (!sessionStorage.category) return callback();


        server.signedRequest('getLesson', {lesson: {id: sessionStorage.category}}, function (response) {


               sessionStorage.categoryName =  response.data.name;
            callback();


        }); // Callback on error


    }


    function setupLesson(callback) {



        var token = getURLToken();


       if(token) if(token.category) sessionStorage.category = token.category;





        setupCategory(function(){



                var platform = store.get('platform');

                if(!token) {
                    $scope.menu.new.lesson();
                    return callback();
                }


                if(!$scope.platform) {
                $scope.menu.new.lesson();
                return callback();
            }



            if(!token.id) {
                $scope.menu.new.lesson();
                return callback();
            }


            if(token.id === "new") {
                $scope.menu.new.lesson();
                return callback();
            }




            getLesson(token.id, function(lessonOnServer){


                if(!lessonOnServer ){
                    $scope.menu.new.lesson();
                    return callback();
                }


                dBstore.get('lesson', token.id, function (lessonOnCache) {


                    if(lessonOnCache){

                        util.debug.info('Cache entry exists');



                        if(lessonOnCache.updated > Date.parse(lessonOnServer.uploaded)){   //If cache is more recent
                            util.debug.info('Cache copy more recent: using cache');
                            workspace.setObject(lessonOnCache, 'lesson');

                            workspace.save(function () {
                                callback();
                            });
                        } else{                                               // If server is more recent than cache
                            util.debug.info("Server copy more recent: Downloading fresh copy");
                            packager.fromServer(token.id, $scope.platform, onsuccess, onerror, onprogress);

                        }




                    }

                    else{

                        util.debug.info('No cache entry');
                        util.debug.info('Downloading from server');
                        packager.fromServer(token.id, $scope.platform, onsuccess, onerror, onprogress);
                    }
                });





            });





        });










        $scope.loadingStatus = "loadLesson";
        util.safeApply($scope);


        function onsuccess(object){

            workspace.setObject(object, "lesson");
             callback();

        }

        function onerror(e){

            if(e) console.log(e);
            $scope.menu.new.lesson();
            return callback();
        }




        function onprogress(event){

            $scope.progress = event*2;
            util.safeApply($scope);

        }



        function getLesson(lessonID, callback){


            server.signedRequest('getLesson', {lesson: {id: lessonID}}, function (response) {


                callback(response.data);


            }); // Callback on error


        }


        function getURLToken(){

            var url = window.location.href;


            if(url.split("#").length < 2) return null;

            var token = url.split("#")[1];


            var secret = authenticator.getAuthCredentials().token.split('.')[1];


            var decoded = CryptoJS.AES.decrypt(token, secret).toString(CryptoJS.enc.Utf8);


            try{

                return JSON.parse(decoded);
            } catch(e){
                return null;
            }





        }


    }


    function setupLogin(callback) {


        if(login.session.exists()){

            login.session.signIn(function(){

                console.log(authenticator.getAuthCredentials());
                $scope.username = authenticator.getAuthCredentials().username;
              callback();
            }, function(){

                $scope.loadingStatus = "login";
                util.safeApply($scope);
            });

        } else{

            $scope.loadingStatus = "login";
            util.safeApply($scope);


        }



        $scope.signIn = function () {

            $scope.loadingStatus = "waiting";

            login.login( $scope.username,  $scope.password, function(){
                $scope.username = '';
                $scope.password = '';

                callback();

            }, function(message){

                switch (message){
                    case  "User not validated":
                        alert("Please validate your account, and then try loggging in");
                        break;
                    case  "Incorrect credentials":
                        alert("Username or password incorrect. Please try again");
                        break;
                    default :
                        alert("A problem occurred while contacting the server. Please try again in a little while");
                        break;
                }

                $scope.loadingStatus = "login";

                util.safeApply($scope);
            });

        };






    }


    function setupUIEventHandlers(callback) {


        function setupPanelSize() {

            var setPanelSize = function () {


                var bufferx = 80;
                var buffery = 50;

                var sideMenuWidth = parseInt(jQuery(".side-menu").css("width").split("p")[0]);
                var topBarHeight = parseInt(jQuery("#banner").css("height").split("p")[0]);

                var width = jQuery(window).width() - sideMenuWidth - bufferx;

                var height = jQuery(window).height() - topBarHeight - buffery;


                if (width < 800) width = 800;
                if (height < 600) height = 600;

                jQuery("#new-item-panel").css("width", width + "px").css("height", height + "px").css("top", "100px").css("left", "265px");


            };


            setPanelSize();

            jQuery(window).resize(setPanelSize);


        }

        function setupReOrder() {


            var reOrderReady = true;

            function reOrder() {


                if (reOrderReady) {


                    reOrderReady = false;

                    var newOrder = [];

                    $.each($("#lesson-list").children(), function (index, value) {


                        var id = value.id;
                        var new_index = id.split("_")[1];

                        newOrder.push(new_index);


                    });


                    workspace.current.object.json.items = courseManager.reOrder.items(workspace.current.object.json.items, newOrder);

                    workspace.setObject(workspace.current.object, "lesson");


                    util.safeApply($scope, function () {

                        reOrderReady = true;

                    });

                }

            }


            jQuery("#lesson-list").sortable({
                axis: "y",
                containment: "document",
                stop: reOrder
            });
        }



        setupPanelSize();

        setupReOrder();

        callback();


    }


    function setupLanguage(callback) {



        i18n.init({ fallbackLng: 'en' }, function(err, t) {
            $scope.t = t;


            $scope.lang = i18n.lng();


        //    $scope.language = i18next.lng();

            window.t = t;

            callback();


        });


    }


    function setupMenu(callback) {


        $scope.menu = {

            new: {

                lesson: function () {

                    if (workspace.current.object.json) {


                        if (confirm("Save current lesson and open new one?")) {

                            workspace.save(function () {
                                workspace.setObject(courseManager.create.lesson(), "lesson");
                                editor.set(workspace.current.object);
                                $scope.screen = 'file';
                            });
                        }

                    } else {
                        workspace.setObject(courseManager.create.lesson(), "lesson");
                        editor.set(workspace.current.object);
                        $scope.screen = 'file';
                    }

                    util.safeApply($scope);
                },

                item: {
                    inLesson: function () {

                        $scope.screen = 'item';
                    },
                    inCourse: {
                        inLesson: function (index) {
                        }
                    }

                }


            },


            create: function () {

                util.debug.log('Created new ' + workspace.current.type);

                $scope.screen = '';

                $scope.workspace.save(function () {

                    util.safeApply($scope, statusHandler.notify(t("status.save.success", {object: workspace.current.type}) ));

                }, function () {

                    util.safeApply($scope, statusHandler.warn(t("status.save.error", { object: workspace.current.type})));
                });

            },

            save: function () {

                util.debug.log('Saved ' + workspace.current.type);

                $scope.workspace.save(function () {

                    util.safeApply($scope, statusHandler.notify(t("status.save.success", {object: workspace.current.type})));
                }, function () {

                    util.safeApply($scope, statusHandler.warn(t("status.save.error", {object: workspace.current.type})));
                });


            },


            edit: {

                lesson: function (object) {

                    $scope.screen = 'file';
                    editor.edit(object);

                },

                course: function (object) {


                    editor.edit(object);

                }


            },

            delete: {

                lesson: function () {


                    if (confirm(t("confirm.delete", {object: 'lesson'}))) {

                        $scope.workspace.deleteObject(function () {
                            util.safeApply($scope, statusHandler.notify(t("status.remove.success", {object: 'lesson'})));
                        }, function (event) {
                            util.safeApply($scope, statusHandler.notify(t("status.remove.error", {object: 'lesson'})));
                        });
                    }

                },


                item: function (index) {

                    if (confirm(t("confirm.delete", {object: 'item'}))) {
                        //$scope.workspace.selected.item =
                        $scope.workspace.deleteItem(index);
                        util.safeApply($scope, statusHandler.notify(t("status.remove.success",  {object: 'item'})));

                    }


                }
            },


            open: function (type) {

                $scope.screen = 'library';

                library.getCourses(type, function () {

                    util.safeApply($scope);

                });


            },


            package: function () {
                util.debug.info('loading package for export...');
                $scope.screen = 'package';

            }


        };


        callback();
    }













});

app.controller('EditController', function ($scope, editor, fields) {

    $scope.fields = editor.fields;

    $scope.editor = editor;

    $scope.init = {
        name: true,
        description: true
    };


    $scope.clearName = function(){
        if($scope.init.name){
            $scope.init.name = false;

            $scope.editor.object.json.name = '';
        }
    };

    $scope.clearDescription = function(){
        if($scope.init.description){
            $scope.init.description = false;

            $scope.editor.object.json.description = '';
        }
    };



    $scope.setImage = function(file){

        switch (file.type){
            case "image/jpeg":
            case "image/png":
                $scope.editor.image.blob = file;
                $scope.editor.image.url = URL.createObjectURL(file);
                break;
            default:
                alert("Please upload a proper image file");
                break;

        }


    };

    $scope.languages = [];
    $scope.languageCodes = [];

    $scope.languageCodes = Object.keys(editor.languages);

    Object.keys(editor.languages).forEach(function(code){
        $scope.languages.push({
            code: code,
            name: editor.languages[code]
        });

    });



    $scope.$watch('editor.image.blob', function () {                                 // Here the editor image is being changed

        console.log('successful trigger');

        try{

            if (typeof editor.image.blob.name !== 'undefined'){
                if(editor.object.json.image !== editor.image.blob.name){                      // Delete the old one

                    delete editor.object.files.meta[editor.object.json.image];
                    util.debug.info('Successfully deleted file '+ editor.object.json.image);

                }

                editor.object.json.image = editor.image.blob.name;
                editor.object.files.meta[editor.image.blob.name] = editor.image.blob;

            }



        } catch (e) {

            util.debug.warn('Problem changing  editor image file: ' + e.message);

        }

    });



});



app.controller('profileController', function($scope, statusHandler){


    if (localStorage.profile){
        $scope.profile = JSON.parse(localStorage.profile);
    } else{

        console.log('here');



        var defaultProfile = {
            name: '',
            language: 'en',
            copyright: 'cc'
        };

        $scope.profile = defaultProfile;

        localStorage.profile = JSON.stringify(defaultProfile);

        $scope.$parent.screen = 'profile';

        util.safeApply($scope);


    }


    $scope.submit = function () {


        localStorage.profile = JSON.stringify($scope.profile);

        statusHandler.notify("Updated Profile");

        $scope.close();

    };


    $scope.close = function () {
        $scope.$parent.screen = '';
    };



});

app.controller('ImportController', function ($scope, packager, fileHandler, loader, workspace, statusHandler ) {

    $scope.lastP = 0;


    $scope.$watch('file', function () {

        if(typeof $scope.file != 'undefined'){


            util.debug.info('Importing file....');

            fileHandler.open($scope.file, function (){


                util.debug.log('File successfully opened', false);

                packager.import(fileHandler.getJSON(), fileHandler.type, function (object) {



                    util.debug.info('File successfully imported');
                    statusHandler.notify(t("status.import.success"));

                    workspace.setObject(object, "lesson");

                    workspace.save(function () {
                        util.safeApply($scope);
                        $scope.$parent.screen = '';
                    });






                    $scope.lastP = 0;
                }, function (e) {

                    e = e || {message: ''};

                    util.debug.log('There was a problem importing the file...');


                    statusHandler.warn(t("status.import.error") + ': ' + e.message);
                    util.safeApply($scope);

                }, function (progress) {

                    var rounded = Math.round(progress);

                    if(rounded > $scope.lastP){

                        util.safeApply($scope, function () {
                            $scope.lastP = rounded;});

                        util.debug.log('Progress = '+ $scope.lastP, false);

                    }


                });
            }, function (e) {

                statusHandler.warn(t("status.fileread.error") + ': ' + e.message);

            });

        }
    });

});
app.controller('ReportController', function ($scope, statusHandler, server) {




    $scope.$watch("$parent.screen == 'report'", function () {

        util.safeApply($scope, function () {
            $scope.name ="";
            $scope.description= "";

        });
    });



    util.debug.info('Initializing server report');


    $scope.report = function () {

        var toSend = {
            name: $scope.name,
            description: $scope.description,
            platform: 'courseCreator',
            log: util.debug.report(),
            language: i18n.lng(),
            user: navigator.userAgent
        };


        server.signedRequest('reportError', toSend, function(response){

            util.debug.info('Successfully saved data: ' + response.data);
            statusHandler.notify(t("status.report.success"));
            $scope.$parent.screen = '';


        }, function(){

            statusHandler.warn(t("status.report.error"));
            $scope.$parent.screen = '';

        });

    };


});


app.controller('PublishController', function($scope, workspace, packager, statusHandler){


    $scope.publish = function(){


        console.log(workspace.current.object);


        packager.toServer(workspace.current.object, function () {

            workspace.deleteObject(function(){


                $scope.closing = true;
                util.safeApply($scope);



                setTimeout(function () {

                    window.close();

                }, 1000);



            });




        }, function (e) {

           $scope.$parent.screen = "";
            statusHandler.warn(t("status.publish.error", {object: 'lesson' }));
            $scope.dynamic = 0;
            util.safeApply($scope);


        }, function (p) {


          $scope.dynamic = p;
            util.safeApply($scope);

        });


    };





});

app.controller('editItemController', function ($scope, itemManager, workspace, statusHandler) {




    $scope.previewing = false;

    $scope.preview = function () {
        itemManager.saveItem();
        itemManager.previewItem();
        $scope.previewing = true;
    };

    $scope.edit = function () {

        $scope.label = $scope.getLabel();
        itemManager.editItem();
        $scope.previewing = false;
    };

    $scope.getLabel = function( ){
        return  itemManager.getLabel();
    };



    $scope.$watch('getLabel()', function(){
        $scope.label = itemManager.getLabel();
    });


    $scope.done = function () {



        if(!$scope.previewing){
            itemManager.saveItem();
        }

        itemManager.setLabel($scope.label);

        $scope.$parent.screen = '';
        $scope.previewing = false;


        workspace.save(function () {

            util.safeApply($scope, statusHandler.notify(t("status.save.success", {object: workspace.current.type})));

        }, function () {

            util.safeApply($scope, statusHandler.warn(t("status.save.error", {object: workspace.current.type})));
        });


    };


    $scope.close = function(){
        $scope.$parent.screen = '';
        $scope.previewing = false;
    };
});


app.controller('PackageController', function ($scope, workspace, packager) {

    $scope.button = true;

    $scope.packager = packager;
    $scope.dynamic = 0;

    $scope.filename = 'hello.crs';


    $scope.export = function () {

        util.debug.info('Starting export...');

        $scope.button = false;
        packager.message = t("packager.zip");

        $scope.packager.export(workspace.current.object, workspace.current.type, function (url) {


            var filename = util.replaceAll(workspace.current.object.json.name, ' ', '_');


            switch(workspace.current.type) {
                case 'e-lesson':
                    filename = filename + '.lsn';
                    break;
                case 'course':
                    filename = filename + '.crs';
                    break;
                case  'lesson':
                    filename = filename + '.lsn';
                    break;

            }

            $scope.filename = filename;
            $scope.packager.url = url;
            $scope.dynamic = 0;
            util.safeApply($scope);

        }, function (percent) {

            util.debug.log('Export progress: ' + percent, false);

            $scope.dynamic = percent;

            util.safeApply($scope);
        });

    };

    $scope.close = function () {

        $scope.button = true;
        $scope.packager.clear();
        $scope.$parent.screen = '';
    };


});


app.controller('lessonBarController', function($scope, moduleManager, workspace, itemManager, $timeout){

    $scope.modules = moduleManager.getModules();
    $scope.workspace = workspace;


    $scope.clicked = false;

    $scope.edit = {

        item: function(index){

            if($scope.clicked){

                $scope.$parent.screen = 'edit-item';
                var item = workspace.current.object.json.items[index];
                itemManager.editItem(item);


            } else {

                $scope.clicked = true;
                $timeout(function () {

                    $scope.clicked = false;
                }, 1000);
            }



        }
    };

});

app.controller('newItemController', function($scope, moduleManager, courseManager, workspace, itemManager){

    $scope.modules = moduleManager.getModules();




    $scope.moduleName = function(module){





        if(module.locales){

            var countrySpecific = i18n.lng();
            var general = countrySpecific.split('-')[0];

            if(module.locales[countrySpecific])
               if(module.locales[countrySpecific].name) return module.locales[countrySpecific].name;


            if(module.locales[general])
                if(module.locales[general].name) return module.locales[general].name;


        }




       return module.name;



    };




    $scope.newItem = function(moduleName){


        $scope.$parent.screen = 'edit-item';

        var newItem = {
            label: t('untitled'),
            module: moduleName,
            data: $scope.modules[moduleName].edit.default,
            id: util.genID()
        };


        courseManager.add.item(workspace.current.object, newItem) ;


        itemManager.editItem(newItem);


        util.safeApply($scope);
        util.debug.log('Creating new item of type '  + moduleName);
    };

});


app.directive('file', function(){
    return {
        scope: {
            file: '='
        },
        link: function(scope, el, attrs){
            el.bind('change', function(event){



                console.log('regular');

                var files = event.target.files;
                scope.$parent.file = files[0];
                scope.$apply();

            });
        }
    };
});


app.directive('previewImage', function(){
    return {
        scope: {
            file: '='
        },
        link: function(scope, el, attrs){
            el.bind('change', function(event){


                console.log('preview');

                var files = event.target.files;
                scope.$parent.setImage(files[0]);
                scope.$apply();

            });
        }
    };
});