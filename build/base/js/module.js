/*====================================================================================================================================

Project: dot Learn

Application: HTML Web app

Version: v0.6.1

Author: Sam Bhattachatryya

File: module.js

Description: AngularJS module for handing modules. After loading each of the installed Modules, each module is assigned it's own iframe,
a sandboxed environment in which modules have full control. Angular will automatically define the iframes, which are determined by the 
module this.frames.  This code manages the lifecycle of the modules, interacting with each module according to the module API.

For more information, check out the info about the module API on the Wiki and /or the Readme


=======================================================================================================================================*/


angular.module('module', []).
	value('installLocation', 'modules/').
	value('modules', {}).
	value('installedModules', ['html', 'image', 'audio', 'video', 'quiz-multiple', 'fill-in-the-blank', 'pdf', 'audio-slideshow']).
 factory('moduleManager', function (installedModules, installLocation, modules) {
    

  return {

         getInstalled: function () {
            return installedModules;
         },


	    getModules: function () {
			return modules;
		},

         importModules: function (callback) {                      // For a list of the installed modules, lazyload their js files from their install location


          callback = callback || function(){};

          async.each(installedModules, function (installedModuleName, next) {

              var location = installLocation + installedModuleName + '/module.json';

              $.getJSON(location, function (json) {
               modules[installedModuleName] = json;
                 next();

              }).fail(function () {
                  next("There was a problem importing module: " + installedModuleName);
              });

          }, function(err){

              if(err) util.debug.warn(err);
              callback();
          });

         }

  };
  
 }).
 run(function (moduleManager) {                        // Import and init all the functions after loading
     
           moduleManager.importModules();

 });


