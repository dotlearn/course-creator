/**
 * Created by sam on 1/16/15.
 */var app = angular.module('app', [])
    .config( [
        '$compileProvider',
        function( $compileProvider )
        {
            $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|blob):/);
            // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
        }
    ]).
  controller('mainController', function ($scope) {


        $scope.editCaption  = false;
        $scope.editSubtitle = false;
        $scope.editDescription = false;

        i18n.init(function (err, t) {
            $scope.t = t;
            $scope.$apply();
        });



        $scope.data = {

            file: {}
        };

        $scope.item = {

        };

        $scope.file = {};


        $scope.setFile = function (file) {

            $scope.file = file;



            if((typeof $scope.item.data.location !==  "undefined") && ($scope.item.data.location !== "")  ){
                $scope.fileLoader.item.remove($scope.item.data.location);
                $scope.item.data.location = "";
            }



            switch (file.name.split(".")[1]){

                case "png":
                case "jpg":
                    $scope.item.data.location = file.name;
                    $scope.fileLoader.item.set(file.name, file);

                    $scope.preview();
                    break;
                default:
                    console.log("invalid file format");
                    return null;
                    break;
            }



        };




        window.onLoad = function (dotLearn) {

            $scope.item.data = dotLearn.data;

            $scope.fileLoader = dotLearn.file;

            $scope.$apply();

            $scope.preview();

        };



        window.onFinished = function () {

            return JSON.parse(JSON.stringify($scope.item.data));
        };


        $scope.preview = function () {

            if((typeof $scope.item.data.location !==  "undefined") && ($scope.item.data.location !== "")  ){

                var file = $scope.fileLoader.item.get($scope.item.data.location);



                    $scope.image = URL.createObjectURL(file);

                    $scope.$apply();



            }
        };


    }).directive('previewImage', function(){
        return {
            scope: {
                previewImage: '='
            },
            link: function(scope, el, attrs){
                el.bind('change', function(event){
                    var files = event.target.files;
                    var file = files[0];

                    scope.file = file;

                    scope.$parent.setFile(file);

                    scope.$apply();

                });
            }
        };
    });;