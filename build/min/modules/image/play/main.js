/**
 * Created by sam on 1/18/15.
 */



window.onLoad = function (app) {

    try {

        var panel =document.getElementById('content');
        panel.innerHTML = '';

        var header = document.getElementById('header');
        header.innerHTML = '';

        var subtitle = document.getElementById('subtitle');
        subtitle.innerHTML = '';


        var explanation = document.getElementById('explanation');
        explanation.innerHTML = '';

        if(typeof app.data.caption !== 'undefined'){
            header.appendChild(document.createTextNode(app.data.caption));
        } else{

            header.appendChild(document.createTextNode(app.data.location.split('/').pop()));
        }


        subtitle.innerHTML = app.data.subtitle ||  '';
        explanation.innerHTML = app.data.description || '';


        app.file.get(app.data.location, function (data) {

            var img = document.createElement('img');

            img.src = URL.createObjectURL(data);


            panel.appendChild(img);


        }, function (e) {

        });
    } catch (e) {

        console.log('Something is up here - image not loading');
    }


};




window.onNext = function () {




};