/**
 * Created by sam on 1/18/15.
 */


window.onLoad = function (app) {




    var data = app.data;


    document.getElementById('header').appendChild(document.createTextNode(data.prompt));





    if(typeof app.user.get() == 'string'){


        document.getElementById("fill-in-the-blank-response").value= app.user.get();
    }


    if(data.image){
        app.file.get(app.data.image, function (data) {

            var img = document.getElementById("quiz-image");

            img.src = URL.createObjectURL(data);


        }, function (e) {

        });


    } else{
        document.getElementById("quiz-image").style.display="none";

    }


    if(data.hint.image){
        app.file.get(app.data.hint.image, function (data) {

            var img = document.getElementById("hint-image");

            img.src = URL.createObjectURL(data);


        }, function (e) {

        });


    } else{
        document.getElementById("hint-image").style.display="none";

    }





};


window.onNext = function (app) {


    var response = document.getElementById('fill-in-the-blank-response');

    app.user.set(response.value);


};
