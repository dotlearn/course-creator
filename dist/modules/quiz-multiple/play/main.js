/**
 * Created by sam on 1/18/15.
 */



    window.onLoad = function (dotLearn) {



var user = dotLearn.user.get();
var quiz = dotLearn.data;
        console.log(user);

document.getElementById("quiz-text").appendChild(document.createTextNode(quiz.prompt));



var answers = document.getElementById("answers");



quiz.options.forEach(function(option, index){               // for each quiz option, add an input

    var row = document.createElement('div');
    row.className = 'quiz-option';

    var input = document.createElement('input');
    input.type = 'radio';
    input.value = index;
    input.name = 'quiz';

    if(typeof user != 'undefined'){



        if(user == index){
            input.checked = true;
        }
    }

    row.appendChild(input);
    row.appendChild(document.createTextNode("  "+option.text));
    answers.appendChild(row);
});



//document.getElementById('content').appendChild(div);

        if(dotLearn.data.image){
            dotLearn.file.get(dotLearn.data.image, function (data) {

                var img = document.getElementById("quiz-image");

                img.src = URL.createObjectURL(data);


            }, function (e) {

            });


        } else{
            document.getElementById("quiz-image").style.display="none";

        }


        if(dotLearn.data.hint.image){
            dotLearn.file.get(dotLearn.data.hint.image, function (data) {

                var img = document.getElementById("hint-image");

                img.src = URL.createObjectURL(data);


            }, function (e) {

            });


        } else{
            document.getElementById("hint-image").style.display="none";

        }


        /*

*/


    };


window.onNext = function (dotLearn) {

    var responses = document.forms['quiz']['quiz'];

    for (var i=0; i<responses.length; i++){
        if (responses[i].checked){
            dotLearn.user.set(i);
        }
    }


};