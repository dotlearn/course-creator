/**
 * Created by sam on 1/18/15.
 */


window.onLoad = function (app) {

    app.blocker.block();

    var content = document.getElementById('content');

    content.innerHTML = '';

    var loader = document.createElement('img');
    loader.src = 'loading.gif';
    loader.alt = '';
    loader.width = "600";
    loader.height = "415";
    loader.style.display = "block";
    loader.style.margin = "auto";
    loader.style.top = "5%";
    content.appendChild(loader);


    app.file.get(app.data.location, function (data) {


        var src;
        var type;
        var video;


        if(data.type.split(';').length > 1) type = data.type.split(';')[0];
        else type = data.type;



        if(type !== "video/mp4"){

            var tag = document.createElement('script');

            tag.src = "https://www.youtube.com/iframe_api";
            document.head.appendChild(tag);
            video = document.createElement('div');


            var player;

            var ID;
            if (app.data.source) {
                if (app.data.source.source) {

                    if(app.data.source.source.split('.be/').length >1) ID = app.data.source.source.split('.be/')[1];
                    if(app.data.source.source.split('youtube.com/watch?v=').length >1) ID = app.data.source.source.split('youtube.com/watch?v=')[1];
                }

            }


            ID = ID || 'M7lc1UVf-VE';

            window.onYouTubeIframeAPIReady= function () {

                player = new YT.Player('player', {
                    height: '415',
                    width: '600',
                    videoId: ID,
                    events: {
                        'onReady': onPlayerReady
                    }
                });
            };

            function onPlayerReady(event) {
                event.target.playVideo();
            }



        } else{



            src = URL.createObjectURL(data);
            video = document.createElement("video");
            video.controls = true;


        }

        video.width = "600";
        video.height = "415";
        video.style.marginRight = "auto";
        video.style.position= "relative";
        video.style.marginLeft = "auto";
        video.style.top= "30px";
        video.style.display = "block";

        video.src = src;

        video.id = 'player';




        content.innerHTML = '';

        content.appendChild(video);




        app.blocker.unblock();

    }, function (e) {

    });




};




window.onNext = function (app) {


    if(typeof x.pause !== 'undefined') {
        x.pause();

    }
};