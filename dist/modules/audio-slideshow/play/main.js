/**
 * Created by sam on 1/16/15.
 */







window.onLoad = function (dotLearn) {


  var slideShow  = new audioSlideshow(dotLearn);


    document.getElementById("back").addEventListener("click", slideShow.back);
    document.getElementById("next").addEventListener("click", slideShow.next);



    window.back = function(){
        console.log("hey");
        slideShow.back();
    }






};


window.onNext = function(){



};






function audioSlideshow(app, config){

    var self = this;

    //Setup Variables
    self.current = 0;
    self.slides = app.data.slides;

    self.config = config || {};


    var delay = self.config.delay || 300;

    self.config.transition = self.config.transition || true;




    console.log(URL.createObjectURL);


    var div = document.getElementById('container');



   //Control functions

    function setSlide(index){
        self.current = index;



        if((index > (self.slides.length-1)) || (index < 0))  return null;


        var slide = self.slides[index];



        var image;
        var mp3;


        set('status', generateStatus());


        app.file.get(slide.image, function(data){

            set('img-container', generateImg(data));

            app.file.get(slide.audio, function(data){

                fadeIn(function(){
                    set('audio-container', generateAudio(data));
                });


            });

        });

    }


    function set(id, object){
        var target = document.getElementById(id);
        target.innerHTML = "";
        target.appendChild(object);
    }



    function generateStatus(){
       return document.createTextNode((self.current+1) + " / " + self.slides.length);
    }



    function generateAudio(audioBlob){

        var audio = document.createElement('audio');
        audio.src = URL.createObjectURL(audioBlob);
        audio.controls = true;
        audio.autoplay = true;
        audio.className = "slide-audio";
        audio.onended = next;
        return audio;
    }

    function generateImg(imBlob){

        if(document.getElementById("currentImg")) delete document.getElementById("currentImg");


        var image = document.createElement('img');

        image.src = URL.createObjectURL(imBlob);
        image.width = "700";

        image.className = "slide";
        image.id = "currentImg";


        return image;

    }



    function init(){
        setSlide(0);
    }




    function next(){
        if(self.current < (self.slides.length-1)) {
            fadeOut(function () {
                setSlide(self.current + 1);
            });
        }
    }

    function fadeOut(callback){
       if(self.config.transition) $("#currentImg").fadeOut(self.delay, callback);
        else callback();
    }
    function fadeIn(callback){
        if(self.config.transition) $("#currentImg").fadeIn(self.delay, callback);
        else callback();
    }


    function back(){
        if(self.current > 0){
            fadeOut(function () {
                setSlide(self.current -1);
            });
        }
    }



    this.set = setSlide;
    this.next = next;
    this.back = back;



    init();



}