/**
 * Created by sam on 1/16/15.
 */
var app = angular.module('app', []).
    controller('mainController', function ($scope) {


        i18n.init(function (err, t) {
            $scope.t = t;
            $scope.$apply();
        });




        $scope.item = {

        };

        window.onLoad = function (dotLearn) {

            $scope.fileLoader = dotLearn.file;


            $scope.item.data = dotLearn.data;

            $scope.$apply();

        };




        $scope.setImage = function (file) {

            $scope.file = file;

            var ext = file.name.split(".")[1];

            switch(ext){
                case "jpg":
                case "png":

                    if((typeof $scope.item.data.image !==  "undefined") && ($scope.item.data.image !== "")  ){
                        $scope.fileLoader.item.remove($scope.item.data.image);
                        $scope.item.data.image = "";
                    }

                    $scope.item.data.image = file.name;
                    break;

                default:
                    return null;
                    console.log("invalid file type");

            }

            $scope.fileLoader.item.set(file.name, file);

        };


        $scope.setHintImage = function (file) {


            $scope.hintFile = file;

            var ext = file.name.split(".")[1];

            switch(ext){
                case "jpg":
                case "png":

                    if((typeof $scope.item.data.hint.image !==  "undefined") && ($scope.item.data.hint.image !== "")  ){
                        $scope.fileLoader.item.remove($scope.item.data.hint.image);
                        $scope.item.data.hint.image = "";
                    }

                    $scope.item.data.hint.image = file.name;
                    break;

                default:
                    return null;
                    console.log("invalid file type");

            }

            $scope.fileLoader.item.set(file.name, file);


        };






        window.onFinished = function () {


            return JSON.parse(JSON.stringify($scope.item.data));
        };


    }).directive('previewImage', function(){
        return {
            scope: {
                previewImage: '='
            },
            link: function(scope, el, attrs){
                el.bind('change', function(event){
                    var files = event.target.files;
                    var file = files[0];

                    scope.file = file;

                    scope.$parent.setImage(file);

                    scope.$apply();

                });
            }
        };
    }).directive('hintImage', function(){
        return {
            scope: {
                hintImage: '='
            },
            link: function(scope, el, attrs){
                el.bind('change', function(event){
                    var files = event.target.files;
                    var file = files[0];

                    scope.file = file;

                    scope.$parent.setHintImage(file);

                    scope.$apply();

                });
            }
        };
    });