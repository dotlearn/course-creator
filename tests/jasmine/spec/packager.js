describe('Unit:  Packager', function() {

var file;


    var img;
    beforeEach(module('app'));

    beforeEach(function (done) {


        zip.workerScriptsPath = "js/lib/zip/";
        file = a.lsn;
        file.name = 'a.lsn';

        inject(function (dBstore) {
            dBstore.init(function(){
                done();
            });

        });



       img = document.createElement('img')

    });

    it('should have a  packager service', function () {

        inject(function (packager) {
                 expect(packager).toBeDefined();
        });
    });


    it('should be able to import an existing lesson, and extract the contents correctly', function (done) {


        inject(function (packager, workspace, fileHandler) {



              fileHandler.open(file, function () {


                  packager.import(fileHandler.getJSON(), fileHandler.type, function (object) {



                      expect(object.json.name).toEqual("Prime Factorization");



                      var item = object.json.items[0];

                      expect(object.files.res[item.id]).toBeDefined();


                      var image = object.files.res[item.id][item.data.location];

                      expect(image).toBeDefined();


                      img.src = URL.createObjectURL(image);

                      document.body.appendChild(img);
                      done();


                  });
              });


        });
    });

    it('should be able to properly export a lesson before being imported again', function (done) {


        inject(function (packager, workspace, fileHandler, courseManager) {

            workspace.setObject(courseManager.create.lesson(), 'lesson');
            workspace.current.object.files.meta['onesies.jpg'] = img1;


            workspace.current.object.files.res['0'] = {
                'earth.png': img2
            };

            workspace.current.object.json.image = 'onesies.jpg';

            var lesson = workspace.current.object.json;


            lesson.items[0] = {
                module: 'image',
                id: '0',
                data: {
                    location: 'earth.png'

                },
                label: ''
            };


            packager.export(workspace.current.object, 'lesson', function (url, blob) {


                blob.name = "a.lsn";

                fileHandler.open(blob, function(){




                    console.log(fileHandler.getJSON());
                    expect(fileHandler.getJSON().name).toEqual("Title");

                    var meta = fileHandler.setPreview(function(){

                        var img3 = document.createElement('img');
                        img3.src = fileHandler.preview.image;
                        document.body.appendChild(img3);


                        fileHandler.getFile('res/0/earth.png', function(result){

                            expect(result).toBeDefined();

                            done();
                        });


                    });



                });

            });



        });
    });








});