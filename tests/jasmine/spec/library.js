describe('Unit:  Library', function() {

    //var module = angular.module('app');

    beforeEach(module('app'));

    beforeEach(function (done) {

        inject(function (dBstore) {

            dBstore.init(function(){
                done();
            });


        });

    });

    it('should have a library service', function () {

        inject(function (library) {
            expect(library).toBeDefined();
        });
    });


    it('should return an empty list for no courses', function (done) {


        inject(function (library) {

            expect(library.courses).toBeDefined();
            expect(library.courses.length).toEqual(0);

            library.getCourses('course',  function () {

                expect(library.courses.length).toEqual(0);
                done();
            });

        });
    });


    it('should be able to add, and then remove a course', function (done) {

        inject(function (library, workspace, courseManager) {
            expect(library.courses.length).toEqual(0);
            workspace.setObject(courseManager.create.course(), 'course');
            workspace.save(function () {

                library.getCourses('course', function () {

                    expect(library.courses.length).toEqual(1);

                   library.remove(0, function(){

                       library.getCourses('course', function () {

                           expect(library.courses.length).toEqual(0);
                           done();
                       });


                   });


                });

            });

        });

    });


    it('should be able to create add multiple courses, and then remove multiple courses', function (done) {


        inject(function (library, workspace, courseManager) {
            expect(library.courses.length).toEqual(0);
            workspace.setObject(courseManager.create.course(), 'course');
            workspace.save(function () {



                workspace.setObject(courseManager.create.course(), 'course');
                workspace.save(function () {


                    workspace.setObject(courseManager.create.course(), 'course');
                    workspace.save(function () {



                        library.getCourses('course', function () {

                            expect(library.courses.length).toEqual(3);





                            library.remove(1, function(){
                                library.getCourses('course', function () {
                                    expect(library.courses.length).toEqual(2);


                                    library.deleteAll(function(){



                                        library.getCourses('course', function () {
                                            expect(library.courses.length).toEqual(0);

                                            done();

                                        });

                                    });

                                });

                            });

                        });



                    });

                });

            });



        });

    });

/*
 library.getCourses('course', function () {

 expect(library.courses.length).toEqual(1);

 library.remove(0, function(){

 library.getCourses('course', function () {

 expect(library.courses.length).toEqual(0);
 done();
 });


 });


 });
 */




});