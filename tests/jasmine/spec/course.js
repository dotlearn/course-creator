describe('Unit:  CourseHandler', function() {

    beforeEach(module('app'));


    beforeEach(function (done) {

        inject(function(authenticator){

            authenticator.setAuthCredentials({
               'username': 'Sam'
            });

            done();
        });

    });



    beforeEach(function (done) {

         i18n.init({ fallbackLng: 'en' }, function(err, t) {
             window.t = t;

             done();
         });



     });


    it('should have a course handler function', function () {

        inject(function (courseManager) {
            expect(courseManager).toBeDefined();
        });
    });


    it('should be able to create an e-course with the appropriate fields', function () {

        inject(function ( courseManager) {

             var course = courseManager.create.course();

            expect(course.json.name).toBeDefined();
            expect(course.json.description).toBeDefined();
            expect(course.json.image).toBeDefined();
            expect(course.json.lessons).toBeDefined();
            expect(course.id).toBeDefined();
            expect(course.json.lessons).toBeDefined();
            expect(course.json.version).toBeDefined();
        });
    });

    it('should be able to create an e-lesson with the appropriate fields', function () {

        inject(function ( courseManager) {

            var course = courseManager.create.lesson();

            expect(course.json.name).toBeDefined();
            expect(course.json.description).toBeDefined();
            expect(course.json.image).toBeDefined();
            expect(course.id).toBeDefined();
            expect(course.json.items).toBeDefined();
            expect(course.json.version).toBeDefined();
        });

    });


});


/*
// Test setup procedure
// setup Language
// Login procedure
// Event Handlers
// Set Platform
// Set Lesson


// Course
// Test re-ordered



// Switch everything to Lesson


// Test common:
-  Load the modules
- Get module "Document"
- Expect module JSON
- Load a new module
- Call the function
- Call the end function

// Test items







// Test Common
// - Util function
// Auth
// Debug
// dotelarn
// file
// loader
//login
//moudule
//server
//status
//store
//util
// version










*/