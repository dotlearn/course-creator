/**
 * Created by sam on 8/5/15.
 */


describe('Unit: File', function() {

    beforeEach(module('app'));

    it('should be able to load a .lsn file', function (done) {

        inject(function(fileHandler){


            fileHandler.open(a.lsn, function(){
                expect(fileHandler.getJSON()).toBeDefined();

                expect(fileHandler.getBlob()).toBeDefined();

                expect(fileHandler.getBlob().size).toBeGreaterThan(100);

                done();

            });



        });



    });


    it('should be able to generate a preview', function (done) {

        inject(function(fileHandler){


            fileHandler.open(a.lsn, function(){

                fileHandler.setPreview(function(preview){



                    expect(preview).toBeDefined();

                    expect(preview.image).toBeDefined();
                    expect(preview.name).toBeDefined();


                    expect(fileHandler.preview).toBeDefined();

                    expect(preview.image).toEqual(fileHandler.preview.image);
                    expect(preview.name).toEqual(fileHandler.preview.name);


                    done();



                });


            });



        });


    });


    it('should be able to retrieve a resource from the file', function (done) {


        inject(function(fileHandler) {

            fileHandler.open(a.lsn, function () {

                fileHandler.getFile('lesson.json', function (file) {

                    expect(file).toBeDefined();

                    expect(file.size).toBeGreaterThan(100);

                    expect(file.type).toBe("text/plain");


                    done();

                });


            });

        });





    });





});


