/**
 * Created by sam on 8/5/15.
 */
describe('Unit: Loader', function() {

    beforeEach(module('app'));

    it('should be able to load a zip file', function () {


        inject(function(loader){
           loader.load(a.lsn);


            var file = loader.getBlob();

            expect(file).toBeDefined();

            expect(file.name).toBeDefined();
        });



    });


    it('should be able to extract the zip entries from the file', function (done) {


        inject(function(loader){
            loader.load(a.lsn);


            loader.getEntries(function(entries){

                expect(entries).toBeDefined();

                expect(entries.length).toBeGreaterThan(0);

                done();

            }, function(){


            });


        });



    });


    it('should be able to retrieve a resource from the file', function (done) {


        inject(function(loader){
            loader.load(a.lsn);


            loader.retrieveResource('lesson.json', function(file){

                expect(file).toBeDefined();

                expect(file.size).toBeGreaterThan(100);

                expect(file.type).toBe("text/plain");


                done();

            });


        });



    });





});


