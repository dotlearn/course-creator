describe('Unit:  Workspace', function() {

    //var module = angular.module('app');

    beforeEach(module('app'));

    beforeEach(function (done) {

        inject(function(dBstore){

            dBstore.init(function () {
                done();
            });
        });


    });

    it('should have a workspace service', function () {

        inject(function (workspace) {
            expect(workspace).toBeDefined();
        });
    });




    it('should be able to create and delete an e-lesson', function (done) {

        inject(function (workspace, courseManager, dBstore) {

            expect(workspace.current.object.json).toBeUndefined();

            dBstore.list('lesson', function(files){

                expect(files.length).toEqual(0);

                workspace.setObject(courseManager.create.lesson(), 'lesson');

                expect(workspace.current.object.json).toBeDefined();
                workspace.save(function(){

                    dBstore.list('lesson', function(files) {

                        expect(files.length).toEqual(1);


                        workspace.deleteObject(function () {

                            dBstore.list('lesson', function(files) {

                                expect(files.length).toEqual(0);
                                done();
                            });

                        });

                    });


                });


            } );

        });
    });


    it('should be able to create and delete items in an e-lesson', function () {



        inject(function (workspace, courseManager, itemManager) {

            workspace.setObject(courseManager.create.lesson(), 'lesson');

            var lesson = workspace.current.object.json;

            expect(lesson.items.length).toEqual(0);




            courseManager.add.item(workspace.current.object, {
                label: 'Untitled',
                module: 'moduleName',
                data: 'default',
                id: util.genID()
            });

            lesson.items[0].data = 0;

            courseManager.add.item(workspace.current.object, {
                label: 'Untitled',
                module: 'moduleName',
                data: 'default',
                id: util.genID()
            });

            lesson.items[1].data = 1;

            courseManager.add.item(workspace.current.object, {
                label: 'Untitled',
                module: 'moduleName',
                data: 'default',
                id: util.genID()
            });

            lesson.items[2].data = 2;

            expect(lesson.items.length).toEqual(3);



            workspace.deleteItem(1);

            expect(lesson.items.length).toEqual(2);

            expect(lesson.items[1].data).toEqual(2);



        });
    });



});