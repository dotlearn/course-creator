describe('Unit:  Lifecycle', function() {

    beforeEach(module('app'));

    var ctrl;
    var scope;

    var $httpBackend;


    beforeEach(

        inject(function($controller, $rootScope, $injector){

            scope = $rootScope.$new();
            ctrl = $controller('UIController', {
                $scope: scope
            });



            $httpBackend = $injector.get('$httpBackend');
            $httpBackend.whenPOST('http://lpaasbackend-teachx.rhcloud.com/validate').respond(200, '');


    }));


    beforeEach(function (done) {

        inject(function(dBstore){
            dBstore.init(function(){
                done();
            })
        })

    });







    it('should be able to create, save and then delete a course', function (done) {

        inject(function (workspace, courseManager, dBstore, editor) {

            expect(workspace.current.object.json).toBeUndefined();


            dBstore.list('course', function(files){

                expect(files.length).toEqual(0);

                setTimeout(function(){


                    scope.menu.new.lesson();

                    expect(workspace.current.object.json).toBeDefined();
                    expect(workspace.current.type).toEqual('lesson');




                    scope.menu.create();


                    setTimeout(function() {



                        dBstore.list('lesson', function (files) {

                            expect(files.length).toEqual(1);


                            workspace.deleteObject(function () {

                                dBstore.list('lesson', function (files) {

                                    expect(files.length).toEqual(0);
                                    done();
                                });

                            });

                        });


                    },  200);



                }, 200);



            } );

        });
    });


});


