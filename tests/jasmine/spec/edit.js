describe('Unit:  Editor', function() {

    //var module = angular.module('app');
    beforeEach(module('app'));


    it('should have an editor service', function () {

        inject(function (editor) {
            expect(editor).toBeDefined();
        });
    });


    it('should set a new e-lesson to the editor workspace', function () {

        inject(function ( courseManager, editor) {

            editor.set( courseManager.create.lesson());


            expect(editor.object.json).toBeDefined();

        });
    });


    it('should set the default blob for an existing e-lesson without an image', function () {

        inject(function (editor,  courseManager) {

              var lesson = courseManager.create.lesson();

              editor.edit(lesson);


            expect(editor.object.json).toBeDefined();

        });

    });

    it('should import an existing e-lesson into the editor, and preserve the image', function () {

        inject(function (editor,  courseManager, workspace) {

            var lesson = courseManager.create.lesson();

            lesson.json.image ='earth.png';
            lesson.files.meta['earth.png'] = img2;


            workspace.setObject(lesson, 'e-lesson');

            editor.edit(lesson,  'e-lesson');

            expect(editor.image.url).not.toEqual('icons/teachx-512.png');
            expect(editor.object.json).toBeDefined();

        });

    });

    /*
    it('should be able to create a lesson with the appropriate fields', function () {

        inject(function ( courseManager) {

            var lesson = courseManager.new.lesson();


            expect(lesson.json).toBeUndefined();;


            expect(lesson.description).toBeDefined();
            expect(lesson.image).toBeDefined();
            expect(lesson.items).toBeDefined();
            expect(lesson.id).toBeUndefined();;
            expect(lesson.version).toBeUndefined();;
        });
    });

*/
});